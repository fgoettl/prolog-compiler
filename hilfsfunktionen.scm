;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Hilfsfunktionen
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Entfernt fuehrenden Whitespace aus einem String.
(define (cut-whitespace-string x)
  (list->string (cut-whitespace (string->list x))))

;Entfernt fuehrenden Whitespace aus einer Liste von Chars.
(define (cut-whitespace x)
  (cond ((null? x) x)
        ((char-whitespace? (car x)) (cut-whitespace (cdr x)))
		(else x)))
	
; Berechnet die neue Klammerungstiefe nach Ueberschreiten von char.	
(define (new-depth char old_depth)
  (cond ((equal? char #\() (add1 old_depth))
		((equal? char #\)) (subtract1 old_depth))
		((equal? char #\[) (add1 old_depth))
		((equal? char #\]) (subtract1 old_depth))
		(else old_depth)))
		
; Wie new-depth aber mit Token als Eingabe.
(define (new-depth-tokens token old_depth)
  (cond ((equal? (car token) 'LBracket) (add1 old_depth))
        ((equal? (car token) 'RBracket) (subtract1 old_depth))
		(else old_depth)))

; Aktualisiert, ob man sich aktuell in einem Literal Atom befindet.
(define (new-in-literal char old_in-literal)
  (cond ((equal? char #\') (not old_in-literal))
		(else old_in-literal)))

; Testet, ob das naechste Element in der Liste eine oeffnende Klammer ("(")ist.
(define (next-is-lbracket? x)
  (cond ((null? x) #f)
        ((equal? (car x) #\() #t)
		(else #f)))

; Addiert 1.
(define (add1 x)
  (+ x 1))

; Subtrahiert 1.
(define (subtract1 x)
  (- x 1))
  
; Bestimmt, ob es sich bei einem Komma in Klammerungstiefe x um eine Konjunktion ("','") oder ein inneres Komma (trennt Argumente eines Funktors) handelt.
(define (kind-of-komma x)
  (cond ((equal? x 0) (cons 'Atom_ "','"))
        (else (cons 'InnerKomma ","))))

; Gibt das erste Element einer Liste zurueck, falls es existiert. Ansonsten #f.
(define (safe-car x)
  (cond ((null? x) #f)
         (else (car x))))

; Verwandelt die Liste von Chars x in einen String, der die Chars in umgekehrter Reihenfolge enthaelt.
(define (reverse-string x)
  (list->string (reverse x)))

; Gibt zu einer Liste l eine Liste von Paaren aufeinanderfolgender Elemente von l zurueck. Z. B.: (1 2 3 4) -> ((1 . 2) (2 . 3) (3 . 4))
(define (consecutive-pairs l)
  (cond ((< (length l) 2) '())
        (else (cons (cons (first l) (second l)) (consecutive-pairs (cdr l))))))

; Beschreibt der gegebene Token-Vector einen einfachen Term?
(define (vector-simple-term? token-vector)
  (cond ((not (equal? (vector-length token-vector) 1)) #f)
	    ((equal? (car (vector-ref token-vector 0)) 'Atom_) #t)
	    ((equal? (car (vector-ref token-vector 0)) 'Cut) #t)
	    ((equal? (car (vector-ref token-vector 0)) 'AtomBr) #t)
	    ((equal? (car (vector-ref token-vector 0)) 'Variable) #t)
	    ((equal? (car (vector-ref token-vector 0)) 'Number) #t)
		(else #f)))

; Besitzt der Token-Vektor Aussenklammern?
(define (enclosed-by-brackets? token-vector)
  (let ((len (vector-length token-vector)))
    (cond ((< len 2) #f)
	      ((and (equal? (car (vector-ref token-vector 0)) 'LBracket) (equal? (car (vector-ref token-vector (subtract1 len))) 'RBracket) (inside-brackets? token-vector 1 (- len 2) 1)) #t)
		  (else #f))))
		  
; Bestimmt, ob die Klammerungstiefe zwischen start und end 0 erreicht.
(define (inside-brackets? token-vector start end starting_depth)
  (cond ((equal? starting_depth 0) #f)
        ((equal? start end) #t)
		(else (let ((new-depth (new-depth-tokens (vector-ref token-vector start) starting_depth)))
		        (inside-brackets? token-vector (add1 start) end new-depth)))))

; Handelt es sich bei x und y um den Gleichen Namen eines Atoms? (Darf sich um "'" unterscheiden.)
(define (equal-atom-identifier? x y)
  (or (equal? x y) (equal? x (string-append "'" y "'")) (equal? (string-append "'" x "'") y)))

; Ist der Assoziativitaetstyp x ein Infix-Typ?
(define (infix-type? x)
  (or (equal? x 'xfx) (equal? x 'yfx) (equal? x 'xfy)))

; Ist der Assoziativitaetstyp x ein Praefix-Typ?
(define (praefix-type? x)
  (or (equal? x 'fx) (equal? x 'fy)))
  
; Ist der Assoziativitaetstyp x ein Postfix-Typ?
(define (postfix-type? x)
  (or (equal? x "xf") (equal? x 'yf)))

; Ist der Term x (gegeben als Syntaxbaum) zusamengesetzt?
(define (complex-term? x)
  (list? x))
  
; Ist der Term x (gegeben als Syntaxbaum) einfach?
(define (simple-term? x)
  (not (complex-term? x)))

; Ist der Term x eine Variable?
(define (var? x)
  (and (pair? x) (equal? (car x) 'Variable)))

; Ist die Variable var in den Substitutionen subst gebunden? Falls ja, wird die entsprechende Substitution zurueckgegeben. Ansonsten #f.
(define (bound? var subst)
  (cond ((equal? (length subst) 0) #f)
        ((equal? var (car (car subst))) (car subst))
		(else (bound? var (cdr subst)))))

; Ist der Term ein Atom?
(define (atom? x)
  (and (pair? x) (or (equal? (car x) 'Atom) (equal? (car x) 'Cut))))
  
; Ist der Term eine Zahlenkonstante?
(define (number-term? x)
  (and (pair? x) (equal? (car x) 'Number)))

; Ist der Term atomar?
(define (atomic? x)
  (or (atom? x) (number-term? x)))
  
; Handelt es sich bei x und y um das gleiche Atom?
(define (equal-atom? x y)
  (and (equal? (car x) (car y)) (equal-atom-identifier? (cdr x) (cdr y))))
  
; Gibt zurueck, durch was var in subst substituiert ist.
(define (lookup var subst)
  (cdr (bound? var subst)))

; Erweitert subst um die Substitution (var->val).
(define (extend-subst var val subst)
  (cons (cons var val) subst))
  
; Fordert Input, bis dieser entweder nur Enter oder ein Semicolon gefolgt von Enter ist. Gibt im ersten Fall #f, im zweiten #t zurueck.
(define (expect-enter-or-semicolon)
  (let ((input (read-line)))
    (cond ((equal? input ";") #t)
	      ((equal? input "") #f)
		  (else (begin (display "Enter or Semicolon!") (expect-enter-or-semicolon))))))
		  
; Gibt die r-vars mitsamt belegungen aus.
(define (output-r-vars r-vars subst)
  (output-r-vars-rek r-vars subst))

; Setzt output-r-vars rekursiv um.
(define (output-r-vars-rek r-vars subst)
  (cond ((null? r-vars) #t)
        (else (begin (pretty-print (car r-vars)) (display "=") (pretty-print (apply-subst (car r-vars) subst)) (display "    ") (output-r-vars-rek (cdr r-vars) subst))))) 

; Gibt term schoener formatiert aus.
(define (pretty-print term)
  (cond ((complex-term? term) (begin (pretty-print (car term)) (display "(") (pretty-print-args (cdr term)) (display ")")))
        (else (display (cdr term)))))

; Gibt die Argumente eines Terms schoener formatiert aus.
(define (pretty-print-args terms)
  (cond ((equal? (length terms) 1) (pretty-print (car terms)))
        (else (begin (pretty-print (car terms)) (display ", ") (pretty-print-args (cdr terms))))))
		
; Extrahiert aus einer Liste l an Substitutionen die Namen der Variablen, die substituiert werden.
(define (first_elements l)
  (cond ((null? l) '())
        (else (cons (car (car l)) (first_elements (cdr l))))))

; Gibt term schoener formatiert und in einer eigenen Zeile aus.
(define (pretty-print-in-line term)
  (begin (newline) (pretty-print term) (newline)))
	
; Ist der term eine Frage?	
(define (question? term)
  (and (complex-term? term) (equal? (cdr (first term)) "?-")))
  
; Ist der term eine Regel?
(define (rule? term)
  (and (complex-term? term) (equal-atom? (car term) (cons 'Atom ":-"))))

; Ist der Term ein Fakt?
(define (fact? term)
  (or (atom? term) (and (complex-term? term) (not (rule? term)) (not (question? term)))))
		
; Gibt Paar aus Hauptfunktor und dessen Stelligkeit zurueck.
(define (main-functor clause)
  (cond ((atom? clause) (cons clause 0))
        ((fact? clause) (cons (car clause) (subtract1 (length clause))))
		((rule? clause) (main-functor (second clause)))))
		
; Sind die beiden Funktoren gleich?
(define (equal-functor? func1 func2)
  (and (equal-atom? (car func1) (car func2)) (equal? (cdr func1) (cdr func2))))

; Fuegt el in Menge s ein, falls es noch nicht enthalten ist. (Als Mengen werden einfach Listen verwendet, in denen keine Duplikate vorkommen.)
(define (set-insert el s)
  (cond ((null? s) (list el))
        ((equal? el (car s)) s)
		(else (cons (car s) (set-insert el (cdr s))))))

; Vereinigt zwei Mengen. (Als Mengen werden einfach Listen verwendet, in denen keine Duplikate vorkommen.)
(define (set-pair-union s1 s2)
  (cond ((null? s1) s2)
        (else (set-pair-union (cdr s1) (set-insert (car s1) s2)))))

; Vereinigt eine Liste von Mengen. (Als Mengen werden einfach Listen verwendet, in denen keine Duplikate vorkommen.)
(define (set-union l)
  (cond ((null? l) '())
        (else (set-pair-union (car l) (set-union (cdr l))))))

; Schnitt von zwei Mengen. (Als Mengen werden einfach Listen verwendet, in denen keine Duplikate vorkommen.)
(define (set-intersection s1 s2)
  (cond ((null? s1) '())
        ((set-contains? s2 (car s1)) (cons (car s1) (set-intersection (cdr s1) s2)))
		(else (set-intersection (cdr s1) s2))))

; Ist el in der Menge s enthalten? (Als Mengen werden einfach Listen verwendet, in denen keine Duplikate vorkommen.)
(define (set-contains? s el)
  (cond ((null? s) #f)
        ((equal? (car s) el) #t)
		(else (set-contains? (cdr s) el))))

; Mengendifferenz s1 ohne s2
(define (set-difference s1 s2)
  (cond ((null? s1) '())
        ((not (set-contains? s2 (car s1))) (cons (car s1) (set-difference (cdr s1) s2)))
		(else (set-difference (cdr s1) s2))))

; Symmetrische Differenz von zwei Mengen. (Als Mengen werden einfach Listen verwendet, in denen keine Duplikate vorkommen.)
(define (set-symmetric-dif s1 s2)
  (set-pair-union (set-difference s1 s2) (set-difference s2 s1)))

; Ist der Term eine Konjunktion?
(define (conjunction? term)
  (and (complex-term? term) (equal-atom-identifier? (cdr (first term)) "','")))
  
; Resultat ist Paar aus (p1 vereinigt mit p2 vereinigt mit (t1 geschnitten mit t2)) und (Symmetrische Differenz von t1 und t2).
(define (merge-perm-temp p1 t1 p2 t2)
  (cons (set-pair-union p1 (set-pair-union p2 (set-intersection t1 t2))) (set-symmetric-dif t1 t2)))
  
; Berechnet die Position von el in der Liste l.
(define (position el l)
  (position-rek el l 1))
(define (position-rek el l n)
  (cond ((null? l) #f)
        ((equal? el (car l)) n)
        (else (position-rek el (cdr l) (add1 n)))))
		

(define (split-list-of-pairs l)
  (cond ((null? l) (cons '() '()))
        (else (let ((rest (split-list-of-pairs (cdr l))))
				(cons (cons (car (car l)) (car rest)) (cons (cdr (car l)) (cdr rest)))))))
  