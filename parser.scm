;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Symbole
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define upper-case-characters (list #\A #\B #\C #\D #\E #\F #\G #\H #\I #\J #\K #\L #\M #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z))
(define lower-case-characters (list #\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m #\n #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z))
(define digit-characters (list #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9))
(define sign-characters (list #\+ #\- #\* #\/ #\\ #\~ #\^ #\< #\> #\: #\. #\? #\@ #\# #\$ #\& #\= #\x003B))

(define variable-start-characters (cons #\_ upper-case-characters))
(define variable-interior-characters (cons #\_ (append upper-case-characters lower-case-characters digit-characters)))
(define atom-signs-start-characters sign-characters)
(define atom-signs-interior-characters (cons #\_ sign-characters))
(define atom-lettersanddigits-start-characters lower-case-characters)
(define atom-lettersanddigits-interior-characters (cons #\_ (append lower-case-characters digit-characters)))
(define number-start-characters digit-characters)
(define number-interior-characters digit-characters)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Operatoren; eine hoehere Zahl entspricht geringerer Praezedenz.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define predefined-operators (list 
  (list 1200 ":-"  'xfx)
  (list 1200 "-->" 'xfx)
  (list 1200 "?-" 'fx)
  (list 1100 ";" 'xfy)
  (list 1000 "," 'xfy)
  (list 900 "\\+" 'fy)
  (list 700 "=" 'xfx)
  (list 700 "\\=" 'xfx)
  (list 700 "==" 'xfx)
  (list 700 "\\==" 'xfx)
  (list 700 "@<" 'xfx)
  (list 700 "@=<" 'xfx)
  (list 700 "@>" 'xfx)
  (list 700 "@>=" 'xfx)
  (list 700 "is" 'xfx)
  (list 700 "<" 'xfx)
  (list 700 "=<" 'xfx)
  (list 700 ">" 'xfx)
  (list 700 ">=" 'xfx)
  (list 700 "=.." 'xfx)
  (list 500 "+" 'yfx)
  (list 500 "-" 'yfx)
  (list 400 "*" 'yfx)
  (list 400 "/" 'yfx)
  (list 400 "//" 'yfx)
  (list 400 "mod" 'yfx)))
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Zerlege Code in einzelne Clauses.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Zerlegt einen String in eine Liste von Strings, die jeweils eine Clause (ohne Punkt) beinhalten.
(define (decompose-code-into-clauses code-string)
  (let* ((code-string-wows (cut-whitespace-string code-string))
         (code-list (string->list code-string-wows))
         (split-point (first-period-to-split code-list 0 0 #f)))
    (cond ((null? code-list) '())
	      ((not split-point) (list code-string-wows))
		  (else (cons  (substring code-string-wows 0 split-point) (decompose-code-into-clauses (substring code-string-wows (add1 split-point) (string-length code-string-wows))))))))

; Findet bei Aufruf mit pos=0 depth=0 in-literal=#f den ersten Punkt ("."), der das Ende einer Clause markiert. Dies ist der erste Punkt, der nicht geklammert oder in einem Literal (eingeschlossen von "'") ist und nicht unmittelbar vor einer oeffnenden Klammer ("(") steht.
(define (first-period-to-split char-list pos depth in-literal)
  (cond ((null? char-list) #f)
		((and (equal? (car char-list) #\.) (equal? depth 0) (not in-literal) (not (next-is-lbracket? (cdr char-list)))) pos)
		(else (first-period-to-split (cdr char-list) (add1 pos) (new-depth (car char-list) depth) (new-in-literal (car char-list) in-literal)))))
		  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Zerlegung des Programmcodes einer Clause, implementiert in etwa wie ein Automat - die Zustaende sind als Funktionen modelliert.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Zerlegt den Code einer einzelnen Clause (als String) in Tokens, also Bezeichner, Klammern, etc.
(define (decompose-clause code-string)
  (let ((code-list (string->list code-string)))
    (dc-recursive code-list 0)
  )
)

; Zerlegt den Code einer einzelnen Clause (als Liste) rekursiv in Tokens, also Bezeichner, Klammern, etc.
; todo ist der verbleibende Code, depth die aktuelle Klammerungstiefe.
(define (dc-recursive todo depth)
  (let ((cut-todo (cut-whitespace todo)))
    (cond ((null? cut-todo) '())
	      (else (let ((next-char (car cut-todo)))
		          (cond ((member next-char variable-start-characters) (dc-in-variable (list next-char) (cdr cut-todo) depth))
				        ((member next-char atom-signs-start-characters) (dc-in-atom-signs (list next-char) (cdr cut-todo) depth))
						((member next-char atom-lettersanddigits-start-characters) (dc-in-atom-lettersanddigits (list next-char) (cdr cut-todo) depth))
						((equal? next-char #\') (dc-in-atom-literal (list next-char) (cdr cut-todo) depth))
						((member next-char number-start-characters) (dc-in-number (list next-char) (cdr cut-todo) depth))
						((equal? next-char #\[) (cons (cons 'LSBracket "[") (dc-recursive (cdr cut-todo) (add1 depth))))
						((equal? next-char #\]) (cons (cons 'RSBracket "]") (dc-recursive (cdr cut-todo) (subtract1 depth))))
						((equal? next-char #\() (cons (cons 'LBracket "(") (dc-recursive (cdr cut-todo) (add1 depth))))
						((equal? next-char #\)) (cons (cons 'RBracket ")") (dc-recursive (cdr cut-todo) (subtract1 depth))))
						((equal? next-char #\|) (cons (cons 'Vertical "|") (dc-recursive (cdr cut-todo) depth)))
						((equal? next-char #\!) (cons (cons 'Cut "!") (dc-recursive (cdr cut-todo) depth)))
						((equal? next-char #\,) (cons (kind-of-komma depth) (dc-recursive (cdr cut-todo) depth)))))))))

; todo und depth sind wie bei dc-recursive. In current werden die Zeichen des aktuellen Tokens angesammelt. Die folgenden Funktionen verhalten sich alle sehr aehnlich.
(define (dc-in-variable current todo depth)
  (let ((next-char (safe-car todo)))
    (cond ((not next-char) (list (cons 'Variable (reverse-string current))))
	      ((member next-char variable-interior-characters) (dc-in-variable (cons next-char current) (cdr todo) depth))
	      (else (cons (cons 'Variable (reverse-string current)) (dc-recursive todo depth))))))
	  
(define (dc-in-atom-signs current todo depth)
  (let ((next-char (safe-car todo)))
    (cond ((not next-char) (list (cons 'Atom_ (reverse-string current))))
	      ((member next-char atom-signs-interior-characters) (dc-in-atom-signs (cons next-char current) (cdr todo) depth))
	      ((equal? next-char #\() (cons (cons 'AtomBr (reverse-string current)) (dc-recursive todo depth)))
		  (else (cons (cons 'Atom_ (reverse-string current)) (dc-recursive todo depth))))))

(define (dc-in-atom-lettersanddigits current todo depth)
  (let ((next-char (safe-car todo)))
    (cond ((not next-char) (list (cons 'Atom_ (reverse-string current))))
	      ((member next-char atom-lettersanddigits-interior-characters) (dc-in-atom-lettersanddigits (cons next-char current) (cdr todo) depth))
	      ((equal? next-char #\() (cons (cons 'AtomBr (reverse-string current)) (dc-recursive todo depth)))
		  (else (cons (cons 'Atom_ (reverse-string current)) (dc-recursive todo depth))))))

(define (dc-in-atom-literal current todo depth)
  (let ((next-char (safe-car todo)))
    (cond ((not next-char) (error "Uncompleted literal atom."))
	      ((equal? next-char #\') (let ((nnext-char (safe-car (cdr todo))))
		                            (cond ((not nnext-char) (cons (cons 'Atom_ (reverse-string (cons next-char current))) (dc-recursive (cdr todo) depth)))
									      ((equal? nnext-char #\() (cons (cons 'AtomBr (reverse-string (cons next-char current))) (dc-recursive (cdr todo) depth)))
										  (else (cons (cons 'Atom_ (reverse-string (cons next-char current))) (dc-recursive (cdr todo) depth))))))
	      (else (dc-in-atom-literal (cons next-char current) (cdr todo) depth)))))

(define (dc-in-number current todo depth)
  (let ((next-char (safe-car todo)))
    (cond ((not next-char) (list (cons 'Number (reverse-string current))))
	      ((member next-char number-interior-characters) (dc-in-number (cons next-char current) (cdr todo) depth))
	      (else (cons (cons 'Number (reverse-string current)) (dc-recursive todo depth))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Expandiert den Syntactic-Sugar fuer Listen. (z. B. [] [a,X] [x,Y|S] [T])
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Entfernt den Syntactic-Sugar fuer Listen aus einem Vektor von Tokens.
(define (expand-list-syntax token-vector)
  (let ((list-details (find-sqbracket-pair token-vector)))
    (cond ((not list-details) token-vector)
	      (else (expand-list-syntax (expand-list token-vector list-details))))))

; Berechnet zu einem gefundenen Paar aus eckigen Klammern eine Liste der relevanten Stellen im Token-Vektor (das sind die oeffnende und schliessende eckige klammern sowie die zu dieser Liste gehoerenden Kommata und Striche ("|")). Gibt #f zurueck, falls kein solches Paar existiert.
(define (find-sqbracket-pair token-vector)
  (let ((result (find-sqb-rek token-vector 0 (vector-length token-vector) 0 '())))
    (cond (result (reverse result))
	      (else #f))))

; Setzt find-sqbracket-pair rekursiv um. pos ist die aktuelle Position im Token-Vektor, len ist die Laenge des Token-Vektors, nesting-depth ist die aktuelle Klammerungstiefe bezueglich eckiger Klammern, in results wird das Ergebnis angesammelt.
(define (find-sqb-rek token-vector pos len nesting-depth results)
  (cond ((and (equal? pos len) (null? results)) #f)
        ((and (equal? pos len) (not (null? results))) (error "Prolog-Syntax-Error: Incorrect List Syntax."))
        (else (let ((next-token-type (car (vector-ref token-vector pos))))
		        (cond ((and (equal? next-token-type 'LSBracket) (equal? nesting-depth 0)) (find-sqb-rek-started token-vector (add1 pos) len (add1 nesting-depth) (cons (cons 'LSBracket pos) results)))
					  (else (find-sqb-rek token-vector (add1 pos) len nesting-depth results)))))))

; Nach dem Start muss auch die Verschachtelung in runden Klammern beachtet werden; Runde und eckige Klammern koennen gemeinsam gezaehlt werden.
(define (find-sqb-rek-started token-vector pos len nesting-depth results)
  (cond ((and (equal? pos len) (null? results)) #f)
        ((and (equal? pos len) (not (null? results))) (error "Prolog-Syntax-Error: Incorrect List Syntax."))
        (else (let ((next-token-type (car (vector-ref token-vector pos))))
		        (cond ((and (equal? next-token-type 'LSBracket) (equal? nesting-depth 0)) (find-sqb-rek-started token-vector (add1 pos) len (add1 nesting-depth) (cons (cons 'LSBracket pos) results)))
				      ((equal? next-token-type 'LSBracket) (find-sqb-rek-started token-vector (add1 pos) len (add1 nesting-depth) results))
					  ((and (equal? next-token-type 'RSBracket) (equal? nesting-depth 1)) (cons (cons 'RSBracket pos) results))
					  ((equal? next-token-type 'RSBracket) (find-sqb-rek-started token-vector (add1 pos) len (subtract1 nesting-depth) results))
				      ((equal? next-token-type 'LBracket) (find-sqb-rek-started token-vector (add1 pos) len (add1 nesting-depth) results))
					  ((equal? next-token-type 'RBracket) (find-sqb-rek-started token-vector (add1 pos) len (subtract1 nesting-depth) results))
					  ((and (equal? next-token-type 'InnerKomma) (equal? nesting-depth 1)) (find-sqb-rek-started token-vector (add1 pos) len nesting-depth (cons (cons 'InnerKomma pos) results)))
					  ((and (equal? next-token-type 'Vertical) (equal? nesting-depth 1)) (find-sqb-rek-started token-vector (add1 pos) len nesting-depth (cons (cons 'Vertical pos) results)))
					  (else (find-sqb-rek-started token-vector (add1 pos) len nesting-depth results)))))))

; Expandiert die durch list-details (erhalten aus find-sqbracket-pair) beschriebene Liste im Token-Vektor.
(define (expand-list token-vector list-details)
  (let* ((adj-pairs (consecutive-pairs list-details))
         (pre-segment (vector-copy token-vector 0 (cdr (first list-details))))
		 (post-segment (vector-copy token-vector (add1 (cdr (last list-details))) (vector-length token-vector))))
    (vector-append pre-segment (expand-list-rek token-vector adj-pairs) post-segment)))
	
; Setzt expand-list rekursiv um. Verwendet dazu Paare aufeinanderfolgender relevanter Stellen. (Die Paare lassen Rueckschluesse darauf zu, wo sich der Teil zwischen den beiden relevanten Stellen in der Liste befindet.) 
(define (expand-list-rek token-vector adj-pairs)
  (let* ((cur-pair (car adj-pairs))
        (first-type (car (car cur-pair)))
		(first-pos (cdr (car cur-pair)))
		(second-type (car (cdr cur-pair)))
		(second-pos (cdr (cdr cur-pair))))
	(cond ((and (equal? first-type 'LSBracket) (equal? second-type 'RSBracket) (equal? second-pos (add1 first-pos))) (vector (cons 'Atom_ "[]"))); Leere Liste
	      ((and (equal? first-type 'LSBracket) (equal? second-type 'RSBracket)) (vector-append (vector (cons 'AtomBr "'.'") (cons 'LBracket "(")) (vector-copy token-vector (add1 first-pos) second-pos) (vector (cons 'InnerKomma ",") (cons 'Atom_ "[]") (cons 'RBracket ")") ))); Einelementige Liste
	      ((and (equal? first-type 'Vertical) (equal? second-type 'RSBracket)) (vector-copy token-vector (add1 first-pos) second-pos)); Tail einer Liste
	      ((and (equal? first-type 'InnerKomma) (equal? second-type 'RSBracket)) (vector-append (vector (cons 'AtomBr "'.'") (cons 'LBracket "(")) (vector-copy token-vector (add1 first-pos) second-pos) (vector (cons 'InnerKomma ",") (cons 'Atom_ "[]") (cons 'RBracket ")") ))); Letztes Element einer Liste
		  (else (vector-append (vector (cons 'AtomBr "'.'") (cons 'LBracket "(")) (vector-copy token-vector (add1 first-pos) second-pos) (vector (cons 'InnerKomma ",")) (expand-list-rek token-vector (cdr adj-pairs)) (vector (cons 'RBracket ")"))))))); Ein Element, welches nicht das letzte in der Liste ist


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Parser fuer den Token-Vektor einer Clause. Erstellt einen Syntaxbaum zum gegebenen Token-Vektor.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Recursive-Descent-Parser, der den Token-Vector in einen Syntaxbaum verwandelt. Ein einfacher Term ist dabei einfach durch das Atom/die Variable gegeben. Ein zusammengesetzter Term ist eine Liste, bestehen aus dem Funktor und den Argumenten in Reihenfolge.
(define (parse-token-vector token-vector)
  (cond ((vector-simple-term? token-vector) (let* ((token (vector-ref token-vector 0))
                                                   (token-type (car token))
												   (token-content (cdr token)))
										      (cond ((or (equal? token-type 'Atom_) (equal? token-type 'AtomBr)) (cons 'Atom token-content))
											        (else token))))
        ((enclosed-by-brackets? token-vector) (parse-token-vector (vector-copy token-vector 1 (subtract1 (vector-length token-vector)))))
		(else (let* ((outermost-functor-and-pos (find-outermost-functor token-vector))
		             (outermost-functor (car outermost-functor-and-pos))
					 (outermost-pos (cdr outermost-functor-and-pos)))
			    (cond ((not outermost-functor) (error "Prolog-Syntax-Error: Kein Funktor/Operator in zusammengestztem Term."))
				      ((equal? (car outermost-functor) 'AtomBr) (cons (cons 'Atom (cdr outermost-functor)) (parse-functor-arguments token-vector outermost-pos))); Aeusserster Funktor ist nicht als Operator geschrieben
				      (else (let* ((optype (search-operator (cdr outermost-functor) predefined-operators)); Aeusserster Funktor ist als Operator geschrieben
					               (vec-end (subtract1 (vector-length token-vector))))
					               (cond ((and (infix-type? (third optype)) (not (equal? outermost-pos 0)) (not (equal? outermost-pos vec-end))) (list (cons 'Atom (cdr outermost-functor)) (parse-token-vector (vector-copy token-vector 0 outermost-pos)) (parse-token-vector (vector-copy token-vector (add1 outermost-pos) (add1 vec-end)))))
								         ((and (praefix-type? (third optype)) (equal? outermost-pos 0) ) (list (cons 'Atom (cdr outermost-functor)) (parse-token-vector (vector-copy token-vector 1 (add1 vec-end)))))
										 ((and (postfix-type? (third optype)) (equal? outermost-pos vec-end))  (list (cons 'Atom (cdr outermost-functor)) (parse-token-vector (vector-copy token-vector 0 vec-end))))
										 (else (error (string-append "Prolog-Syntax-Error: Bad positioning of Operator: " (cdr outermost-functor))))))))))))

; Parst die Argumente eines Funktors.
(define (parse-functor-arguments token-vector pos)
  (let* ((boundary-points (reverse (get-functor-argument-boundary-points token-vector (list (add1 pos)) (+ pos 2) 1)))
        (boundary-pairs (consecutive-pairs boundary-points))
		(parse-between (lambda (p) (parse-token-vector (vector-copy token-vector (add1 (car p)) (cdr p))))))
		(map parse-between boundary-pairs)))

; Berechnet in points die Positionen im Token-Vektor, die die Argumente des Funktors trennen (Kommata und die schliessende Klammer).
(define (get-functor-argument-boundary-points token-vector points pos depth)
  (let ((token (vector-ref token-vector pos)))
    (cond ((and (equal? (car token) 'InnerKomma) (equal? depth 1)) (get-functor-argument-boundary-points token-vector (cons pos points) (add1 pos) depth))
	      ((and (equal? (car token) 'RBracket) (equal? depth 1)) (cons pos points))
		  (else (get-functor-argument-boundary-points token-vector points (add1 pos) (new-depth-tokens token depth))))))



; Bestimmt im Token-Vektor den Funktor/Operator mit der geringsten Praezedenz und dessen Position. Gibt #f zurueck, falls keiner existiert.
(define (find-outermost-functor token-vector)
  (find-out-func-rec token-vector (cons #f #f) 0 0 (vector-length token-vector)))

; Setzt find-outermost-functor rekursiv um. 
(define (find-out-func-rec token-vector current-highest nesting-depth pos len)
  (cond ((equal? pos len) current-highest)
        (else (let* ((next-token (vector-ref token-vector pos))
		            (next-type (car next-token)))
				(cond ((equal? next-type 'LBracket) (find-out-func-rec token-vector current-highest (add1 nesting-depth) (add1 pos) len))
					  ((equal? next-type 'RBracket) (find-out-func-rec token-vector current-highest (subtract1 nesting-depth) (add1 pos) len))
				      ((and (equal? next-type 'Atom_) (equal? nesting-depth 0)) (find-out-func-rec token-vector (compute-highest current-highest next-token pos) nesting-depth (add1 pos) len))
					  ((and (equal? next-type 'AtomBr) (equal? nesting-depth 0)) (find-out-func-rec token-vector (compute-highest current-highest next-token pos) nesting-depth (add1 pos) len))
					  ((and (equal? next-type 'Cut) (equal? nesting-depth 0)) (find-out-func-rec token-vector (compute-highest current-highest next-token pos) nesting-depth (add1 pos) len))
					  (else (find-out-func-rec token-vector current-highest nesting-depth (add1 pos) len)))))))

; Berechnet, welches der zwei Token zum Funktor mit der geringeren Praezedenz gehoert.
(define (compute-highest old-token-and-pos new-token new-pos)
  (let* ((old-token (car old-token-and-pos))
        (old-prio-and-assoc (prio-and-assoc old-token))
        (new-prio-and-assoc (prio-and-assoc new-token)))
    (cond ((not new-prio-and-assoc) old-token-and-pos); Neues Token ist kein Operator oder Funktor
	      ((not old-prio-and-assoc) (cons new-token new-pos)); Altes Token ist kein Operator oder Funktor
		  ((and (equal? old-prio-and-assoc 'Functor) (equal? new-prio-and-assoc 'Functor)) old-token-and-pos); Beide Token sind Funktoren
	      ((and (list? old-prio-and-assoc) (equal? new-prio-and-assoc 'Functor)) old-token-and-pos); Altes Token ist ein Operator, neues ein Funktor
	      ((and (list? new-prio-and-assoc) (equal? old-prio-and-assoc 'Functor)) (cons new-token new-pos)); Neues Token ist ein Operator, altes ein Funktor
		  ((> (car new-prio-and-assoc) (car old-prio-and-assoc)) (cons new-token new-pos)); Beide Operatoren, Neues hat geringere Praezedenz
		  ((< (car new-prio-and-assoc) (car old-prio-and-assoc)) old-token-and-pos); Beide Operatoren, Neues hat groessere Praezedenz
		  ((or (equal? (third old-prio-and-assoc) 'xfy) (equal? (third old-prio-and-assoc) 'fy)) old-token-and-pos); Beide Operatoren, gleiche Praezedenz, alter Operator kann aussen stehen
		  ((or (equal? (third new-prio-and-assoc) 'yfx) (equal? (third new-prio-and-assoc) 'yf)) (cons new-token new-pos)); Beide Operatoren, gleiche Praezedenz, neuer Operator kann aussen stehen
		  (else (error "Prolog-Syntax-Error: Operator Precedence.")))))
		  
; Berechnet, ob es sich bei dem Token um einen Funktor handelt oder um einen Operator (und berechnet von diesem auch die Praezedenz und Assoziativitaet) oder keins von beidem.
(define (prio-and-assoc token)
  (cond ((not token) #f)
        ((equal? (car token) 'AtomBr) 'Functor)
        ((equal? (car token) 'Atom_) (search-operator (cdr token) predefined-operators))
		((equal? (car token) 'Cut) #f)))

; Sucht den Iddentifier in der Liste der Operatoren.
(define (search-operator identifier operators)
  (cond ((null? operators) #f)
        ((equal-atom-identifier? (second (car operators)) identifier) (car operators))
		(else (search-operator identifier (cdr operators)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Expandiert den Syntactic-Sugar fuer einfache Grammatik-Regeln.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Expandiert im Term den Syntactic-Sugar fuer einfache Grammatik-Regeln.
(define (expand-grammar-syntax term)
  (cond ((and (complex-term? term) (equal-atom-identifier? (cdr (first term)) "-->") (complex-term? (third term)) (equal-atom-identifier? (cdr (first (third term))) "'.'")) (list (second term) (list (cons 'Atom "'.'") (second (third term)) (cons 'Variable "S")) (cons 'Variable "S"))); Es handelt sich um eine Liste auf der rechten Seite.
        ((and (complex-term? term) (equal-atom-identifier? (cdr (first term)) "-->")) (let* ((left-side (second term)); Es handelt sich nicht um eine Liste auf der rechten Seite.
                                                                                            (right-side (third term))
																							(new-left-side (list left-side (cons 'Variable "S0") (cons 'Variable "S")))
																							(new-right-side (expand-grammar-syntax-right-side-rek right-side 0)))
																					    (list (cons 'Atom ":-") new-left-side new-right-side)))
        (else term)))

; Expandiert die rechte Seite einer Regel mit keiner Liste auf der rechten Seite, z. B. "a --> b, c, d.".
(define (expand-grammar-syntax-right-side-rek right-side num)
  (cond ((and (complex-term? right-side) (equal-atom-identifier? (cdr (first right-side)) "','")) (list (cons 'Atom "','") (list (second right-side) (cons 'Variable (string-append "S" (number->string num))) (cons 'Variable (string-append "S" (number->string (add1 num))))) (expand-grammar-syntax-right-side-rek (third right-side) (add1 num)))); Eine Konjunktion
        (else (list right-side (cons 'Variable (string-append "S" (number->string num))) (cons 'Variable "S")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Ersetzt alle Bezeichner von Atomen, die auch ohne Anfuehrungszeichen legitime Bezeichner von Atomen waeren, durch ihre Version ohne einfache Anfuehrungszeichen.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (remove-quotes clause)
  (cond ((and (atom? clause)(equal? (car clause) 'Atom)) (remove-quotes-atom clause))
        ((complex-term? clause) (map remove-quotes clause))
		(else clause)))
(define (remove-quotes-atom atom)
  (let ((name (cdr atom)))
    (cond ((< (string-length name) 2) atom)
	      ((and (equal? (string-ref name 0) #\') (equal? (string-ref name (subtract1 (string-length name))) #\'))
	        (let ((nname (string-copy name 1 (subtract1 (string-length name)))))
	          (cond ((legal-atom-identifier? nname) (cons 'Atom nname))
		            (else atom))))
	  (else atom))))
(define (legal-atom-identifier? s)
  (cond ((equal? (string-length s) 0) #f)
        ((member (string-ref s 0) atom-signs-start-characters) (legal-atom-identifier-rest-signs? (string-copy s 1 (string-length s))))
        ((member (string-ref s 0) atom-lettersanddigits-start-characters) (legal-atom-identifier-rest-lettersanddigits? (string-copy s 1 (string-length s))))
		(else #f)))
(define (legal-atom-identifier-rest-signs? s)
  (cond ((equal? (string-length s) 0) #t)
        (else (and (member (string-ref s 0) atom-signs-interior-characters) (legal-atom-identifier-rest-signs? (string-copy s 1 (string-length s)))))))
(define (legal-atom-identifier-rest-lettersanddigits? s)
  (cond ((equal? (string-length s) 0) #t)
        (else (and (member (string-ref s 0) atom-lettersanddigits-interior-characters) (legal-atom-identifier-rest-lettersanddigits? (string-copy s 1 (string-length s)))))))
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kompletter Parser.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		

(define (parse-prolog code)
  (let* ((code-split (decompose-code-into-clauses code))
         (code-tokenized (map decompose-clause code-split))
		 (code-l-expanded (map expand-list-syntax (map list->vector code-tokenized)))
		 (code-parsed (map parse-token-vector code-l-expanded))
		 (code-g-expanded (map expand-grammar-syntax code-parsed)))
	(map remove-quotes code-g-expanded)))