;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Pretty-Printing fuer WAM-Code und Linked-WAM
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define label-width 15)
(define (pretty-print-wam wam-code)
  (cond ((null? wam-code) #t)
        (else (begin (pretty-print-wam-line (car wam-code)) (newline) (pretty-print-wam (cdr wam-code))))))
(define (pretty-print-wam-line wam-line)
  (let* ((label (car wam-line))
         (label-size (pretty-print-label label))
		 (code (cdr wam-line)))
	(write-string (make-string (max 1 (- label-width label-size)) #\space)) (pretty-print-wam-line-code code)))
		 
; Schreibt das Label und gibt die geschriebene laenge zurueck.
(define (pretty-print-label label)
  (cond ((equal? label 0) 0)
        ((equal? (car (car label)) 'Atom) (let* ((functor-name (cdr (car label)))
												  (functor-arity (cdr label))
												  (arity-size (printed-size functor-arity)))
											 (write-string functor-name)
											 (write-string "/")
											 (write functor-arity)
											 (+ 1 (string-length functor-name) arity-size)))
		(else (let* ((functor-name (cdr (car (car label))))
		             (functor-arity (cdr (car label)))
					 (arity-size (printed-size functor-arity))
					 (label-num (cdr label))
					 (label-num-size (printed-size label-num)))
				(write-string functor-name)
				(write-string "/")
				(write functor-arity)
				(write-string " ")
				(write label-num)
				(+ 2 (string-length functor-name) arity-size label-num-size)))))

(define (pretty-print-register register)
  (begin (display (car register)) (display (cdr register))))

(define (pretty-print-wam-line-code code)
  (case (first code)
    ((try_me_else retry_me_else call) (display (first code)) (write-string " ") (pretty-print-label (second code)))
	((allocate allocate_q set_void unify_void) (display (first code)) (write-string " ") (write (second code)))
	((deallocate trust_me proceed neck_cut deallocate_q) (display (first code)))
	((put_variable put_value get_variable get_value) (display (first code)) (write-string " ") (pretty-print-register (second code))  (write-string " ") (pretty-print-register (third code)))
	((set_variable set_value unify-variable unify_value get_level cut) (display (first code)) (write-string " ") (pretty-print-register (second code)))
	((put_structure get_structure) (display (first code)) (write-string " ") (pretty-print-label (second code)) (write-string " ") (pretty-print-register (third code)))
	(else (display code))))

(define (printed-size num)
  (cond ((equal? num 0) 1)
        (else (exact (ceiling (log (+ 1 num) 10))))))
		
(define (pretty-print-linked-wam linked-wam)
  (let* ((num (vector-length linked-wam))
         (numbering-width (+ (printed-size (subtract1 num)) 2)))
    (pretty-print-linked-wam-rek linked-wam 0 num numbering-width)))
	
(define (pretty-print-linked-wam-rek linked-wam cur num numbering-width)
  (cond ((equal? cur num) #t)
        (else (write cur) (write-string (make-string (- numbering-width (printed-size cur)) #\space)) (pretty-print-linked-wam-line (vector-ref linked-wam cur)) (newline) (pretty-print-linked-wam-rek linked-wam (add1 cur) num numbering-width))))
		
(define (pretty-print-linked-wam-line code)
  (case (first code)
    ((try_me_else retry_me_else) (display (first code)) (write-string " ") (display (second code)))
    ((call) (display (first code)) (write-string " ") (display (second code)) (write-string " ") (display (third code)))
	((allocate allocate_q set_void unify_void) (display (first code)) (write-string " ") (write (second code)))
	((deallocate trust_me proceed neck_cut deallocate_q) (display (first code)))
	((put_variable put_value get_variable get_value) (display (first code)) (write-string " ") (pretty-print-register (second code))  (write-string " ") (pretty-print-register (third code)))
	((set_variable set_value unify-variable unify_value get_level cut) (display (first code)) (write-string " ") (pretty-print-register (second code)))
	((put_structure get_structure) (display (first code)) (write-string " ") (pretty-print-label (second code)) (write-string " ") (pretty-print-register (third code)))
	(else (display code))))