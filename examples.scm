(import (scheme base))
(import (scheme char))
(import (scheme write))
(import (scheme inexact))
(import (srfi 1));lists
(import (srfi 39));parameter objects
(import (srfi 115));regexp
(import (srfi 14));character sets
(import (fgprolog))

;;;;;;;;
;;;;;;;; Zu Abschnitt 2:
;;;;;;;;
(define example-code1
"s(X) :-bc( [a  |X]),num. q(T  ).
q(X ,.(s,Y)).")

(define (parser-test)
  (display (parse-prolog example-code1)))


;;;;;;;;
;;;;;;;; Zu Abschnitt 3:
;;;;;;;;
(define x (pl-interpret "a(b). a(c). b(X) :- a(X)."))
		
		
;;;;;;;;
;;;;;;;; Zu Abschnitt 4:
;;;;;;;;
(define (compiler-test1)
  (pretty-print-wam (compile-database "a(X,c) :- p(X), t(X,Y).
t(q,p).
p(q).")))

(define (compiler-test2)
  (pretty-print-wam (compile-query "?- a(q,c).")))


;;;;;;;;
;;;;;;;; Zu Abschnitt 5:
;;;;;;;;
(define (linker-test)
  (let* ((database (compile-database
			"a(X,c) :- p(X), t(X,Y).
			t(q,p).
			p(q)."))
		 (query (compile-query "?- a(q,c)."))
		 (linked-wam (car (link-wam database query))))
	(pretty-print-linked-wam linked-wam)))

(define (wam-interpreter-test)
  (let* ((database (compile-database
			"a(X,c) :- p(X), t(X,Y).
			t(q,p).
			p(q)."))
		 (query (compile-query "?- a(q,c)."))
		 (x (link-wam database query))
		 (linked-wam (car x))
		 (starting-line (cdr x))
		 (program (wam-interpreter linked-wam starting-line)))
	(program)))


;;;;;;;;
;;;;;;;; Zu Abschnitt 6:
;;;;;;;;
(define (cfg-test)
  (let* ((database (compile-database
"s --> o, p.
o --> n.
o --> n, r.
p --> u.
p --> u, r.
r --> f, n.
n --> a, h.
u --> v.
u --> v, o.
a --> [das].
a --> [dem].
h --> [kind].
h --> [tuch].
h --> [pferd].
v --> [sieht].
v --> [beruehrt].
f --> [mit]."))
		;(query (compile-query "?- s([das, kind, beruehrt, das, pferd, mit, dem, tuch],[])."))
		(query (compile-query "?- s(X,[])."))
		;(query (compile-query "?- s([das, pferd|X],[])."))
		(x (link-wam database query))
		(linked-wam (car x))
		(starting-line (cdr x))
		(program (wam-interpreter linked-wam starting-line)))
	(program)))

(define (arithmetic-test)
  (let* ((database (compile-database
"number([]).
number([a|X]) :- number(X).
add(X,[],X).
add(X,[a|Y],[a|Z]) :- add(X,Y,Z).
multiply(X,[],[]).
multiply(X,[a|Y],Z) :- multiply(X,Y,W), add(X,W,Z).
divides([],X) :-!,equal(X,[]).
divides(X,[]).
divides(X,Y) :- add(X,W,Y), divides(X,W).
equal(X,X).
nequal(X,Y) :- equal(X,Y),!,fail.
nequal(X,Y).
smaller(X,[a|X]).
smaller(X,[a|Y]) :- smaller(X,Y).
composite(X) :- smaller(Y,X), divides(Y,X),nequal(Y,X),nequal(Y,[a]).
prime(X) :- composite(X),!,fail.
prime(X) :- nequal(X,[]),nequal(X,[a]).
"))
		;(query (compile-query "?- prime([a,a,a,a,a,a,a,a])."))
		(query (compile-query "?- number(X), prime(X)."))
		(x (link-wam database query))
		(linked-wam (car x))
		(starting-line (cdr x))
		(program (wam-interpreter linked-wam starting-line)))
	(program)))



; Ausfuehren mit wahlweise (parser-test) oder (x) oder (compiler-test1) oder ...
(cfg-test)



