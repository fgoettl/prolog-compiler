;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Instanziieren der Terme
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Instanziiert den Term, wobei alle Variablen durch frische ersetzt werden und den Cuts IDs gegeben werden, sodass sie beim Backtracking mit ihren Parent-Goals verbunden werden koennen.
; Funktioniert rekursiv. Initialer Aufruf mit previous-substitutions=() cut-ids=() und fresh-number=Zahl derart, dass alle Zahlen >= dieser noch frisch sind.
; Gibt zurueck: (substituted_term substitutions fresh-number cut-ids)
(define (instantiate-term term previous-substitutions fresh-number cut-ids)
  (cond ((simple-term? term) (instantiate-simple-term term previous-substitutions fresh-number cut-ids))
        (else (instantiate-complex-term term previous-substitutions fresh-number cut-ids))))

; Behandelt den Fall eines zusammengesetzten Terms.
(define (instantiate-complex-term term previous-substitutions fresh-number cut-ids)
  (let ((instantiated-arguments (instantiate-arguments (cdr term) previous-substitutions fresh-number cut-ids)))
    (list (cons (car term) (first instantiated-arguments)) (second instantiated-arguments) (third instantiated-arguments) (fourth instantiated-arguments))))

; Behandelt die Argumente eines zusammengesetzten Terms.
(define (instantiate-arguments arguments previous-substitutions fresh-number cut-ids)
  (cond ((null? arguments) (list '() previous-substitutions fresh-number cut-ids))
        (else (let* ((first-instantiated-argument (instantiate-term (car arguments) previous-substitutions fresh-number cut-ids))
		             (rest-instantiated-arguments (instantiate-arguments (cdr arguments) (second first-instantiated-argument) (third first-instantiated-argument) (fourth first-instantiated-argument))))
				(list (cons (first first-instantiated-argument) (first rest-instantiated-arguments)) (second rest-instantiated-arguments) (third rest-instantiated-arguments) (fourth rest-instantiated-arguments))))))

; Behandelt den Fall eines einfachen Terms.
(define (instantiate-simple-term term previous-substitutions fresh-number cut-ids)
  (cond ((var? term) (let ((var-name (cdr term))); Term ist eine Variable
                                                (cond ((equal? var-name "_") (list (cons 'Variable (string-append "IntVar" (number->string fresh-number))) previous-substitutions (add1 fresh-number) cut-ids)); Term ist die anonyme Variable _
												      (else (let ((bound (bound? term previous-substitutions)))
													          (cond (bound (list (cdr bound) previous-substitutions fresh-number cut-ids)); Term ist eine Variable, die schon einen neuen Namen hat
															        (else (list (cons 'Variable (string-append "IntVar" (number->string fresh-number))) (cons (cons term (cons 'Variable (string-append "IntVar" (number->string fresh-number)))) previous-substitutions) (add1 fresh-number) cut-ids)))))))); Term ist eine Variable, die noch keinen neuen Namen hat
        ((equal? (car term) 'Cut) (list (cons 'Cut fresh-number) previous-substitutions (add1 fresh-number) (cons fresh-number cut-ids)))
		(else (list term previous-substitutions fresh-number cut-ids))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Unification (s. Norvi: Unify-bug)
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Berechnet die zum Most General Unifier von x und y gehoerenden Substitutionen aus, wobei subst schon vorhandene Substitutionen sind. Gibt #f aus, falls der nicht existiert.
(define (unify-term x y subst) 
  (cond ((equal? x y) subst)
        ((not subst) #f)
		((and (atomic? x) (atomic? y) (equal-atom? x y)) subst)
        ((var? x) (unify-variable x y subst))
		((var? y) (unify-variable y x subst))
		((or (atomic? x) (atomic? y)) #f)
		((not (equal? (length x) (length y))) #f)
		(else (unify-term (cdr x) (cdr y) (unify-term (car x) (car y) subst)))))

(define (unify-variable var val subst)
  (cond ((equal? var val) subst)
        ((bound? var subst) (unify-term (lookup var subst) val subst))
		((and (var? val) (bound? val subst)) (unify-term var (lookup val subst) subst))
		((occurs-in? var val subst) #f)
		(else (extend-subst var val subst))))

(define (occurs-in? var x subst)
  (cond ((equal? var x) #t)
        ((bound? x subst) (occurs-in? var (lookup x subst) subst))
		((and (complex-term? x) (> (length x) 1)) (or (occurs-in? var (car x) subst) (occurs-in? var (cdr x) subst)))
		((and (complex-term? x) (equal? (length x) 1)) (occurs-in? var (car x) subst))
		(else #f)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Anwenden von Substitutionen
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Wendet die Substitutionen subst auf den Term term an, bis dieser sich nicht mehr aendert, also vollstaendig substituiert wurde.
(define (apply-subst term subst)
  (let* ((sub1 (apply-subst-once term subst))
        (sub2 (apply-subst-once sub1 subst)))
		(cond ((equal? sub1 sub2) sub2)
		      (else (apply-subst sub2 subst)))))
			  
; Wendet alle Substitutionen in subst einmal auf term an (in Umgekehrter Reihenfolge der Liste subst).
(define (apply-subst-once term subst)
  (cond ((null? subst) term)
        (else (apply-single-subst (apply-subst-once term (cdr subst)) (list (car subst))))))

; Wendet eine einzelne Substitution subst auf term an.
(define (apply-single-subst term subst)
  (cond ((complex-term? term) (apply-single-subst-to-all term subst))
        ((and (var? term) (bound? term subst)) (lookup term subst))
		(else term)))

; Wendet eine einzelne Substitution subst auf alle terme in der Liste term an.
(define (apply-single-subst-to-all term subst)
  (cond ((null? term) '())
        (else (cons (apply-single-subst (car term) subst) (apply-single-subst-to-all (cdr term) subst)))))
 
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Backtracking, implementiert in etwa wie ein Kellerautomat - die Zustaende sind als Funktionen modelliert, der Keller wird als Argument mitgefuehrt.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Hauptalgorithmus, der versucht, das initial-goal mithilfe der Datenbank (Vektor von Clauses) db zu beweisen. Das Backtracking hat zwei Hauptfunktionen: Entweder wird versucht, das naechste Ziel zu erfuellen, oder man backtrackt und versucht das vorherige Ziel erneut zu erfuellen.

(define (backtracking-algorithm db initial-goal)
  (let* ((inst-term (instantiate-term initial-goal '() 0 '()))
        (goal (first inst-term))
		(subst (second inst-term))
		(fresh-n (third inst-term))
		(r-vars (first_elements subst)))
    (satisfy-next-goal db (list goal) fresh-n '() r-vars subst)))

; goals sind die noch zu erfuellenden Ziele, fresh-n ist die naechste frische Zahl, btps sind die Backtracking-Points (BTPs), r-vars sind die relevanten Variablen (die, die im initialen Goal enthalten waren), subst ist die Liste der bisher durchgefuehrten Substitutionen.
; Die unterschiedlichen Ziele werden von unterschiedlichen Funktionen behandelt.
(define (satisfy-next-goal db goals fresh-n btps r-vars subst)
  (cond ((null? goals) (output-positive-answer db btps r-vars subst))
        (else (let ((n-goal (car goals)))
		        (cond ((goal-conjunction? n-goal) (handle-goal-conjunction n-goal db (cdr goals) fresh-n btps r-vars subst))
				      ((goal-disjunction? n-goal) (handle-goal-disjunction n-goal db (cdr goals) fresh-n btps r-vars subst))
				      ((goal-cut? n-goal) (handle-goal-cut n-goal db (cdr goals) fresh-n btps r-vars subst))
				      (else (handle-goal-normal n-goal db (cdr goals) fresh-n btps r-vars subst)))))))

; Damit das Backtracking gelingt, enthaelt ein BTP alle wichtigen Informationen: (btp_type, btp_marker, c_goal, goals, fresh-n, subst, cut-ids)  btp_type erlaubt zu erkennen, welche Funktion den BTP uebernehmen muss, btp_marker enthaelt Informationen fuer diese Funktion, c_goal ist das Ziel welches zum Zeitpunkt des BTP erfuellt werden soll, goals sind die anderen verbleibenden Ziele, fresh-n und subst wie oben, cut-ids ist eine Liste mit den IDs der Cuts die bis hierhin "abschneiden" wuerden.
; Die unterschiedlichen BTPs werden von unterschiedlichen Funktionen behandelt.
(define (backtrack db btps r-vars)
  (cond ((null? btps) (begin (display "no") #f))
        (else (let ((last-btp (car btps)))
		        (cond ((btp-normal? last-btp) (handle-btp-normal last-btp db (cdr btps) r-vars))
				      ((btp-disjunction? last-btp) (handle-btp-disjunction last-btp db (cdr btps) r-vars)))))))

; Output-(Input-)Funktion fuer das Backtracking
(define (output-positive-answer db btps r-vars subst)
  (cond ((null? r-vars) (display "yes") #t)
        (else (begin (output-r-vars r-vars subst) (cond ((expect-enter-or-semicolon) (backtrack db btps r-vars)); Nutzer will, dass wir erneut suchen.
                                                        (else #t)))))); Nutzer endet Berechnung.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Funktionen fuer das Backtracking, die die unterschiedlichen Goals behandeln.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Konjunktionen: Hier werden einfach nur die beiden Einzelziele zu Zielen. BTPs muessen nicht erstellt und daher auch nicht behandelt werden.
(define (goal-conjunction? goal)
  (and (complex-term? goal) (equal-atom-identifier? (cdr (first goal)) "','")))
(define (handle-goal-conjunction n-goal db goals fresh-n btps r-vars subst) 
  (satisfy-next-goal db (add-goals (cdr n-goal) goals) fresh-n btps r-vars subst))
(define (add-goals n-goals goals)
  (cond ((null? n-goals) goals)
        (else (cons (car n-goals) (add-goals (cdr n-goals) goals)))))

; Disjunktionen: Bei erster Erfuellung wird das erste Einzelziel hinzugefuegt. Kann einmal erneut erfuellt werden, indem das zweite Ziel hinzugefuegt wird.
(define (goal-disjunction? goal)
  (and (complex-term? goal) (equal-atom-identifier? (cdr (first goal)) "';'")))
(define (handle-goal-disjunction n-goal db goals fresh-n btps r-vars subst) 
  (satisfy-next-goal db (cons (second n-goal) goals) fresh-n (cons (list 'DISJUNCTION 0 n-goal goals fresh-n subst '()) btps) r-vars subst))
(define (handle-btp-disjunction btp db btps r-vars)
  (satisfy-next-goal db (cons (third (third btp)) (fourth btp)) (fifth btp) btps r-vars (sixth btp)))
(define (btp-disjunction? btp)
  (equal? (first btp) 'DISJUNCTION))
  
; Cut: Entfernt einfach alle BTPs, bis (inklusive) dem mit passender CutID.
(define (goal-cut? goal)
  (equal? (car goal) 'Cut))
(define (handle-goal-cut n-goal db goals fresh-n btps r-vars subst)
  (let ((cut-id (cdr n-goal)))
    (satisfy-next-goal db goals fresh-n (pop-btps-until btps cut-id) r-vars subst)))
(define (pop-btps-until btps cut-id)
  (cond ((member cut-id (seventh (car btps))) (cdr btps))
        (else (pop-btps-until (cdr btps) cut-id))))
		
; Kein besonderer Term: Hier haben wir nur die Moeglichkeit, mit einem Fakt oder einer Regel zu matchen. Gelingt das, wird sich die Position der Clause in der db in einem BTP (im btp_marker) gemerkt. Soll das Ziel erneut erfuellt werden, geht die Suche dort weiter. 
(define (handle-goal-normal n-goal db goals fresh-n btps r-vars subst)
  (handle-goal-normal-rek n-goal db goals fresh-n btps r-vars subst 0 (vector-length db)))
(define (handle-btp-normal btp db btps r-vars)
  (handle-goal-normal-rek (third btp) db (fourth btp) (fifth btp) btps r-vars (sixth btp) (second btp) (vector-length db)))
(define (handle-goal-normal-rek goal db goals fresh-n btps r-vars subst clause-number db-length)
  (cond ((>= clause-number db-length) (backtrack db btps r-vars))
        (else (let* ((current-clause (vector-ref db clause-number)) 
		             (applied-clause (applicable? current-clause goal fresh-n subst)))
			    (cond (applied-clause (satisfy-next-goal db (append (third applied-clause) goals) (first applied-clause) (cons (list 'NORMAL (add1 clause-number) goal goals fresh-n subst (fourth applied-clause)) btps) r-vars (second applied-clause)))
				      (else (handle-goal-normal-rek goal db goals fresh-n btps r-vars subst (add1 clause-number) db-length)))))))
(define (btp-normal? btp)
  (equal? (first btp) 'NORMAL))

; Falls die Clause nicht geeignet ist, um das Goal zu zeigen->#f Ansonsten werden die neuen (fresh-n, subst, new_goals_list, cut-ids) zurueckgegeben, die sich durch Anwenden der Clause ergeben.
(define (applicable? clause goal fresh-n subst)
  (cond ((rule? clause) (let* ((inst-clause (instantiate-term clause '() fresh-n '()))
                               (conclusion (second (first inst-clause)))
                               (premise (third (first inst-clause)))
							   (unif (unify-term goal conclusion subst)))
                          (cond (unif (list (third inst-clause) unif (list premise) (fourth inst-clause)))
						        (else #f))))
        (else (let* ((inst-clause (instantiate-term clause '() fresh-n '()))
                     (fact (first inst-clause))
				     (unif (unify-term goal fact subst)))
                (cond (unif (list (third inst-clause) unif '() (fourth inst-clause)))
						        (else #f))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Zusammengesetzter Interpreter.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Parst den Prolog-Code einer Datenbank und gibt eine ausfuehrbare Funktion zurueck, die dann Eingaben entgegen nimmt.
(define (pl-interpret db-code)
  (let* ((database (parse-prolog db-code))
		 (db (list->vector database)))
	(lambda () (ready-and-process db))))
		 
(define (ready-and-process db)
  (begin (newline) (display "Frage oder 'EXIT' eingeben.") (newline)
    (let ((input (read-line)))
      (cond ((equal? input "EXIT") (display "Closing Program"))
	        (else (let* ((query-split (decompose-code-into-clauses input)))
			        (cond ((equal? (length query-split) 1) (let* ((query-tokenized (decompose-clause (first query-split)))
		                                                         (query-l-expanded (expand-list-syntax (list->vector query-tokenized)))
					                                             (query-parsed (parse-token-vector query-l-expanded))
					                                             (query-g-expanded (expand-grammar-syntax query-parsed))
																 (query (remove-quotes query-g-expanded)))
			                                                (cond ((question? query) (begin (backtracking-algorithm db (second query)) (ready-and-process db)))
				                                                  (else (begin (display "Bitte Prolog-Frage eingeben.") (newline) (ready-and-process db))))))
						  (else (begin (display "Genau eine Frage.") (newline) (ready-and-process db))))))))))