(define-library (fgprolog)
  (import (scheme base))
  (import (scheme char))
  (import (scheme write))
  (import (scheme inexact))
  (import (srfi 1));lists
  (import (srfi 39));parameter objects
  (import (srfi 115));regexp
  (import (srfi 14));character sets
  
  (export parse-prolog pl-interpret compile-database compile-query link-wam pretty-print-wam pretty-print-linked-wam wam-interpreter)
  
  (include "parser.scm")
  (include "directinterpreter.scm")
  (include "compiler.scm")
  (include "linker.scm")
  (include "prettyprintwam.scm")
  (include "linker.scm")
  (include "waminterpreter.scm")
  (include "hilfsfunktionen.scm"))