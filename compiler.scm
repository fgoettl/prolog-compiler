;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Teilt die Klauseln nach ihrem Hauptfunktor in Gruppen auf. Liste von Klauseln -> Liste von (Liste mit Funktor als erstem Element und Klauseln mit genau diesem Hauptfunktor)
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (split-by-functor clauses)
  (cond ((null? clauses) '())
        (else (let* ((next-functor (main-functor (car clauses)))
		             (functor-clauses-and-rest (extract-functor clauses next-functor))
		             (functor-clauses (cons next-functor (car functor-clauses-and-rest)))
					 (rest-clauses (cdr functor-clauses-and-rest)))
				(cons functor-clauses (split-by-functor rest-clauses))))))

(define (extract-functor clauses functor)
  (cond ((null? clauses) (cons '() '()))
        (else (let* ((first-clause (car clauses))
		             (first-functor (main-functor first-clause))
					 (rest-extracted (extract-functor (cdr clauses) functor))
					 (rest-matches (car rest-extracted))
					 (rest-not-matches (cdr rest-extracted)))
				(cond ((equal-functor? first-functor functor) (cons (cons first-clause rest-matches) rest-not-matches))
				      (else (cons rest-matches (cons first-clause rest-not-matches))))))))
					  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Uebersetzt nach Hauptfunktoren sortierte Klauseln (s. split-by-functor) in WAM-Code
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; WAM-Code ist eine Liste von WAM-Zeilen, jede ist ein Paar aus (dem Label der Zeile oder 0) und der eigentlichen Anweisung als Liste.
(define (sorted-clauses-to-wam clauses)
  (cond ((null? clauses) '())
        (else (append (functor-code-to-wam (car clauses)) (sorted-clauses-to-wam (cdr clauses))))))

; Der functor-code ist eine Liste mit dem Funktor als erstem Element und Klauseln mit genau diesem Hauptfunktor. Das Label fuer einen Funktor ist genau dieser Funktor
(define (functor-code-to-wam functor-code)
  (let* ((functor (car functor-code))
         (clauses (cdr functor-code))
		 (compiled-clauses (map clause-to-wam clauses))
		 (clause-num (length compiled-clauses)))
	(cond ((equal? clause-num 1) (let* ((compiled-clause (first compiled-clauses)); Nur Label fuer Funktor hinzufuegen
	                                   (first-instruction (cdr (first compiled-clause)))
									   (rest-lines (cdr compiled-clause)))
								  (cons (cons functor first-instruction) rest-lines)))
		  (else (add-choice-instructions compiled-clauses functor 0)))))
		  
; Die Label fuer die Choice-Instructions sind ein Paar aus Funktor und einer Zahl, damit sie eindeutig sind.
(define (add-choice-instructions clauses functor last-label)
  (cond ((equal? last-label 0) (cons (cons functor (list 'try_me_else (cons functor 1))) (append (car clauses) (add-choice-instructions (cdr clauses) functor 1)))); Fuegt die erste Zeile mit try_me_else hinzufuegen
        ((equal? (length clauses) 1) (cons (cons (cons functor last-label) (list 'trust_me)) (car clauses))); Fuegt die trust_me Zeile vor die letzte Option ein
		(else (cons (cons (cons functor last-label) (list 'retry_me_else (cons functor (add1 last-label)))) (append (car clauses) (add-choice-instructions (cdr clauses) functor (add1 last-label)))))))
		

(define (clause-to-wam clause)
  (cond ((rule? clause) (rule-to-wam clause))
        ((fact? clause) (fact-to-wam clause))
		(else (display "Prolog-Error: Klausel ist weder Regel noch Fakt."))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Uebersetzung eines Goals in WAM-Code.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Uebersetzt ein Goal in WAM-Code, bei dem die Vorkommen von Variablen noch nicht behandelt wurden. Wichtig fuer Anwendung bei Regeln und Queries mit mehr als einem Goal. perm und temp sind die fuer den Fakt relevanten permanenten und temporaeren Variablen.
(define (goal-to-wam-x goal perm temp)
  (let ((functor (main-functor goal)))
    (cond ((and (atom? goal) (equal? (car goal) 'Cut)) (list (cons 0 (cdr goal)))); Goal ist ein Cut -> Anweisung ist im zweiten Argument.
	      ((atom? goal) (list (cons 0 (list 'call functor)))); Goal ist ein Atom -> Einfach dieses aufrufen.
	      (else (append (goal-handle-arguments (cdr goal) (subtract1 (length goal)) perm temp (+ (length goal) (length temp)) 1) (list (cons 0 (list 'call functor)))))))); Goal ist kein Atom -> mehrstelliger Funktor -> Argumente putten und aufrufen

; Wie goal-to-wam-x, aber mit Angabe der anzahl der Argumente. (Wichtig fuer Regeln, da sich Head und erstes Goal temp. Var. teilen.)
(define (goal-to-wam-x-len goal len perm temp)
  (let ((functor (main-functor goal)))
    (cond ((and (atom? goal) (equal? (car goal) 'Cut)) (list (cons 0 (cdr goal)))); Goal ist ein Cut -> Anweisung ist im zweiten Argument.
	      ((atom? goal) (list (cons 0 (list 'call functor)))); Goal ist ein Atom -> Einfach dieses aufrufen.
	      (else (append (goal-handle-arguments (cdr goal) (subtract1 len) perm temp (+ len (length temp)) 1) (list (cons 0 (list 'call functor)))))))); Goal ist kein Atom -> mehrstelliger Funktor -> Argumente putten und aufrufen


(define (goal-handle-arguments arguments num-args perm temp next-free next-arg-num)
  (cond ((null? arguments) '())
        (else (let* ((result (goal-handle-argument (car arguments) num-args perm temp next-free next-arg-num))
		             (code-of-first-arg (first result))
					 (n-next-free (second result)))
				(append code-of-first-arg (goal-handle-arguments (cdr arguments) num-args perm temp n-next-free (add1 next-arg-num)))))))
				
(define (goal-handle-argument argument num-args perm temp next-free arg-num)
  (cond ((and (var? argument) (equal? (cdr argument) "_")) (list (list (cons 0 (list 'set_var (cons 'A arg-num)))) next-free)); Nicht beschrieben im WAMBOOK.
        ((var? argument) (let ((code (goal-handle-argument-variable argument num-args perm temp arg-num)))
						   (list code next-free)))
		((atom? argument) (list (list (cons 0 (list 'put_structure (main-functor argument) (cons 'A arg-num)))) next-free))
		((complex-term? argument) (let* ((functor (main-functor argument))
										 (subterms (cdr argument))
										 (handled-subterms (goal-handle-subterms subterms num-args perm temp next-free))
										 (code (first handled-subterms))
										 (n-next-free (second handled-subterms))
										 (after-code (third handled-subterms)))
									(list (append code (list (cons 0 (list 'put_structure functor (cons 'A arg-num)))) after-code) n-next-free)))
		(else (display "Prolog-Fehler: Unbekannte Form eines Arguments."))))

; Gibt (code next-free after-code) zurueck; code ist der Code fuer die Subterme, after-code ist der code, der nach der put_structure Anweisung des Funktors mit den Subtermen ausgefuehrt werden muss.
(define (goal-handle-subterms subterms num-args perm temp next-free)
  (cond ((null? subterms) (list '() next-free '()))
        (else (let* ((result-after-first-subterm (goal-handle-subterm (car subterms) num-args perm temp next-free))
		             (first-code (first result-after-first-subterm))
		             (n-next-free (second result-after-first-subterm))
		             (first-after-code (third result-after-first-subterm))
					 (result-after-rest (goal-handle-subterms (cdr subterms) num-args perm temp n-next-free))
					 (code (append first-code (first result-after-rest)))
					 (nn-next-free (second result-after-rest))
					 (after-code (append first-after-code (third result-after-rest))))
				(list code nn-next-free after-code)))))
					 

; Gibt (code next-free after-code) zurueck.
(define (goal-handle-subterm subterm num-args perm temp next-free)
  (cond ((and (var? subterm) (equal? (cdr subterm) "_")) (list '() next-free (list (cons 0(list 'set_void 1)))))
        ((var? subterm) (let* ((code (goal-handle-subterm-variable subterm num-args perm temp)))
						   (list '() next-free code)))
		((atom? subterm) (list (list (cons 0 (list 'put_structure (main-functor subterm) (cons 'X next-free)))) (add1 next-free) (list (cons 0 (list 'set_value (cons 'X next-free))))))
		((complex-term? subterm) (let* ((functor (main-functor subterm))
										 (n-subterms (cdr subterm))
										 (handled-n-subterms (goal-handle-subterms n-subterms num-args perm temp next-free))
										 (code (first handled-n-subterms))
										 (n-next-free (second handled-n-subterms))
										 (after-code (third handled-n-subterms)))
									(list (append code (list (cons 0 (list 'put_structure functor (cons 'X n-next-free)))) after-code) (add1 n-next-free) (list (cons 0 (list 'set_value (cons 'X n-next-free)))))))
		(else (display "Prolog-Fehler: Unbekannte Form eines Subterms."))))
		
(define (goal-handle-subterm-variable variable num-args perm temp)
  (let* ((variable-perm (set-contains? perm variable)))
	(cond (variable-perm (list (cons 0 (list 'set_var (cons 'Y (position variable perm))))))
	      ((not variable-perm) (list (cons 0 (list 'set_var (cons 'X (+ num-args (position variable temp)))))))
		  (else (display "Prolog-Error: Variable in Subterm fehlerhaft.")))))
		
(define (goal-handle-argument-variable variable num-args perm temp arg-num)
  (let* ((variable-perm (set-contains? perm variable)))
    (cond (variable-perm (list (cons 0 (list 'put_var (cons 'Y (position variable perm)) (cons 'A arg-num)))))
		  ((not variable-perm) (list (cons 0 (list 'put_var (cons 'X (+ num-args (position variable temp))) (cons 'A arg-num)))))
		  (else (display "Prolog-Error: Variable als Argument fehlerhaft.")))))

  

  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Uebersetzung eines Fakts in WAM-Code
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Uebersetzt einen einzelnen Fakt in WAM-Code.
(define (fact-to-wam fact)
  (let* ((perm-and-temp (perm-and-temp-variables fact))
         (perm (car perm-and-temp))
		 (temp (cdr perm-and-temp)))
    (append (fact-handle-variable-occurances (fact-to-wam-x fact perm temp) '()) (list (cons 0 (list 'proceed))))))

; Fakt ohne proceed am Ende und ohne, dass die Vorkommen der Variablen behandelt wurden. Wichtig fuer Anwendung bei Regeln. perm und temp sind die fuer den Fakt relevanten permanenten und temporaeren Variablen.
(define (fact-to-wam-x fact perm temp)
  (let ((functor (main-functor fact)))
    (cond ((atom? fact) '()); Fakt ist ein Atom -> nichts zu pruefen
	      (else (fact-handle-arguments (cdr fact) (subtract1 (length fact)) perm temp (+ (length fact) (length temp)) 1) )))); Fakt ist kein Atom -> mehrstelliger Funktor -> Argumente getten/unifyen
		  
; Wie fact-to-wam-x, aber mit Angabe der anzahl der Argumente. (Wichtig fuer Regeln, da sich Head und erstes Goal temp. Var. teilen.)
(define (fact-to-wam-x-len fact len perm temp)
  (let ((functor (main-functor fact)))
    (cond ((atom? fact) '()); Fakt ist ein Atom -> nichts zu pruefen
	      (else (fact-handle-arguments (cdr fact) (subtract1 len) perm temp (+ len (length temp)) 1) )))); Fakt ist kein Atom -> mehrstelliger Funktor -> Argumente getten/unifyen

; Kuemmert sich darum, bei Variablen die richtigen Anweiungen zu setzen, jenachdem ob die Variable dort das erste mal gesehen wird oder nicht.
(define (fact-handle-variable-occurances code occured)
  (cond ((null? code) '())
        (else (let* ((first-line (car code))
		             (first-label (car first-line))
					 (first-instr (cdr first-line))
					 (first-instr-type (first first-instr)))
				(cond ((equal? first-instr-type 'get_var) (let* ((variable (second first-instr))
				                                                  (rest (cdr (cdr first-instr)))
				                                                  (this-occured (set-contains? occured variable)))
															 (cond (this-occured (cons (cons first-label (append (list 'get_value variable) rest)) (fact-handle-variable-occurances (cdr code) occured)))
															       (else (cons (cons first-label (append (list 'get_variable variable) rest)) (fact-handle-variable-occurances (cdr code) (cons variable occured)))))))
					  ((equal? first-instr-type 'unify_var) (let* ((variable (second first-instr))
				                                                  (rest (cdr (cdr first-instr)))
				                                                  (this-occured (set-contains? occured variable)))
															 (cond (this-occured (cons (cons first-label (append (list 'unify_value variable) rest)) (fact-handle-variable-occurances (cdr code) occured)))
															       (else (cons (cons first-label (append (list 'unify-variable variable) rest)) (fact-handle-variable-occurances (cdr code) (cons variable occured)))))))
					  (else (cons first-line (fact-handle-variable-occurances (cdr code) occured)))))))); Bei den anderen Anweisungen muss nichts getan werden.

(define (fact-handle-arguments arguments num-args perm temp next-free next-arg-num)
  (cond ((null? arguments) '())
        (else (let* ((result (fact-handle-argument (car arguments) num-args perm temp next-free next-arg-num))
		             (code-of-first-arg (first result))
					 (n-next-free (second result)))
				(append code-of-first-arg (fact-handle-arguments (cdr arguments) num-args perm temp n-next-free (add1 next-arg-num)))))))
				
(define (fact-handle-argument argument num-args perm temp next-free arg-num)
  (cond ((and (var? argument) (equal? (cdr argument) "_")) (list '() next-free)); Koennen wir ignorieren (UnifiKation mit anonymer Variable gelingt immer, bindet nichts).
        ((var? argument) (let ((code (fact-handle-argument-variable argument num-args perm temp arg-num)))
						   (list code next-free)))
		((atom? argument) (list (list (cons 0 (list 'get_structure (main-functor argument) (cons 'A arg-num)))) next-free))
		((complex-term? argument) (let* ((functor (main-functor argument))
										 (subterms (cdr argument))
										 (handled-subterms (fact-handle-subterms subterms num-args perm temp next-free))
										 (argument-code (first handled-subterms))
										 (n-next-free (second handled-subterms))
										 (subterm-code (third handled-subterms)))
									(list (append (list (cons 0 (list 'get_structure functor (cons 'A arg-num)))) argument-code subterm-code) n-next-free)))
		(else (display "Prolog-Fehler: Unbekannte Form eines Arguments."))))

; Gibt (argument-code next-free subterm-code) zurueck; subterm-code ist der Code fuer die Subterme, argument-code ist der code, der nach der get_structure Anweisung des Funktors mit den Subtermen ausgefuehrt werden muss.
(define (fact-handle-subterms subterms num-args perm temp next-free)
  (cond ((null? subterms) (list '() next-free '()))
        (else (let* ((result-after-first-subterm (fact-handle-subterm (car subterms) num-args perm temp next-free))
		             (first-argument-code (first result-after-first-subterm))
		             (n-next-free (second result-after-first-subterm))
		             (first-subterm-code (third result-after-first-subterm))
					 (result-after-rest (fact-handle-subterms (cdr subterms) num-args perm temp n-next-free))
					 (argument-code (append first-argument-code (first result-after-rest)))
					 (nn-next-free (second result-after-rest))
					 (subterm-code (append first-subterm-code (third result-after-rest))))
				(list argument-code nn-next-free subterm-code)))))
					 

; Gibt (argument-code next-free subterm-code) zurueck.
(define (fact-handle-subterm subterm num-args perm temp next-free)
  (cond ((and (var? subterm) (equal? (cdr subterm) "_")) (list (list (cons 0(list 'unify_void 1))) next-free '()))
        ((var? subterm) (let* ((code (fact-handle-subterm-variable subterm num-args perm temp)))
						   (list code next-free '())))
		((atom? subterm) (list (list (cons 0 (list 'unify-variable (cons 'X next-free)))) (add1 next-free) (list (cons 0 (list 'get_structure (main-functor subterm) (cons 'X next-free))))))
		((complex-term? subterm) (let* ((functor (main-functor subterm))
										 (n-subterms (cdr subterm))
										 (handled-n-subterms (fact-handle-subterms n-subterms num-args perm temp next-free))
										 (n-argument-code (first handled-n-subterms))
										 (n-next-free (second handled-n-subterms))
										 (n-subterm-code (third handled-n-subterms)))
									(list (list (cons 0 (list 'unify-variable (cons 'X n-next-free)))) (add1 n-next-free) (append (list (cons 0 (list 'get_structure functor (cons 'X n-next-free)))) n-argument-code n-subterm-code))))
		(else (display "Prolog-Fehler: Unbekannte Form eines Subterms."))))
		
(define (fact-handle-subterm-variable variable num-args perm temp)
  (let* ((variable-perm (set-contains? perm variable)))
	(cond (variable-perm (list (cons 0 (list 'unify_var (cons 'Y (position variable perm))))))
	      ((not variable-perm) (list (cons 0 (list 'unify_var (cons 'X (+ num-args (position variable temp)))))))
		  (else (display "Prolog-Error: Variable in Subterm fehlerhaft.")))))
		
(define (fact-handle-argument-variable variable num-args perm temp arg-num)
  (let* ((variable-perm (set-contains? perm variable)))
    (cond (variable-perm (list (cons 0 (list 'get_var (cons 'Y (position variable perm)) (cons 'A arg-num)))))
		  ((not variable-perm) (list (cons 0 (list 'get_var (cons 'X (+ num-args (position variable temp))) (cons 'A arg-num)))))
		  (else (display "Prolog-Error: Variable als Argument fehlerhaft.")))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Uebersetzung einer Regel in WAM-Code
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (rule-to-wam rule)
  (let* ((t (handle-neckcuts rule))
         (rule-x (car t))
		 (neckcut (cdr t))
         (pt (perm-and-temp-variables rule-x))
         (perm (car pt))
		 (temp (cdr pt))
		 (terms-x (rule-to-terms rule-x))
		 (cut-expanded (handle-deepcuts terms-x (length perm)))
		 (terms (car cut-expanded))
		 (cut-precode (cdr cut-expanded))
		 (allocation-lines (cons (cons 0 (list 'allocate (+ (length cut-precode) (length perm)))) cut-precode))
		 (head-and-first-goal-wam-x (head-and-first-goal-to-wam-x (first terms) (second terms) perm temp neckcut))
		 (rest-goals-wam-x (goals-to-wam-x (cdr (cdr terms)) perm temp))
		 (deallocation-lines (list (cons 0 (list 'deallocate))))
	     (wam-x (append allocation-lines head-and-first-goal-wam-x rest-goals-wam-x deallocation-lines)))
	(rule-handle-variable-occurances wam-x '())))
	
; Fuegt den Cuts ihren "cut Yn" code als zweites Argument hinzu und gibt zusaetzlich liste der "get_level Yn" zurueck. num ist die Anzahl der permanenten Variablen in rule -> die Cuts kommen danach.	 
(define (handle-deepcuts terms num)
  (cond ((null? terms) (cons '() '()))
        ((and (atom? (first terms)) (equal? (car (first terms)) 'Cut)) (let* ((n-terms-and-precode (handle-deepcuts (cdr terms) (add1 num)))
																			   (n-terms (car n-terms-and-precode))
																			   (precode (cdr n-terms-and-precode))
																			   (new-precode (cons 0 (list 'get_level (cons 'Y (add1 num)))))
																			   (new-term (cons 'Cut (list 'cut (cons 'Y (add1 num))))))
																		  (cons (cons new-term n-terms) (cons new-precode precode))))
		(else (let* ((n-terms-and-precode (handle-deepcuts (cdr terms) num))
					 (n-terms (car n-terms-and-precode))
					 (precode (cdr n-terms-and-precode)))
				(cons (cons (first terms) n-terms) precode)))))
		 
(define (handle-neckcuts rule)
  (let* ((body (third rule))
         (x (remove-leading-cut body))
		 (n-rule (list (first rule) (second rule) (car x)))
		 (contained-cut (cdr x)))
	(cond (contained-cut (cons (car (handle-neckcuts n-rule)) (list (cons 0 (list 'neck_cut)))))
	      (else (cons rule '())))))

(define (remove-leading-cut body)
  (cond ((and (atom? body) (equal? (car body) 'Cut)) (display "Prolog-Error: Regel ohne Nicht-Cut-Goal."))
        ((and (conjunction? body) (and (atom? (second body)) (equal? (car (second body)) 'Cut))) (cons (third body) #t))
		((conjunction? body) (let* ((x (remove-leading-cut (second body)))
		                            (n-left (car x))
									(contained (cdr x)))
								(cons (list (first body) n-left (third body)) contained)))
		(else (cons body #f))))
		 
(define (head-and-first-goal-to-wam-x head first-goal perm temp neckcut)
  (let* ((head-vars (extract-variables head))
         (head-t (set-intersection head-vars temp))
		 (goal-vars (extract-variables first-goal))
		 (goal-t (set-intersection goal-vars temp))
		 (combined-t (set-pair-union head-t goal-t))
		 (max-length (max (safe-length head) (safe-length first-goal)));notwendig, damit die temp. Var zusammenpassen. (safe-length, da es sich um ein Atom handeln koennte.
		 (head-wam-x (fact-to-wam-x-len head max-length perm combined-t))
		 (goal-wam-x (goal-to-wam-x-len first-goal max-length perm combined-t))) 
	(append head-wam-x neckcut goal-wam-x)))
	
(define (safe-length l)
  (cond ((list? l) (length l))
        (else 0)))
	
(define (goals-to-wam-x goals perm temp)
  (cond ((null? goals) '())
        (else (let* ((first-goal (car goals))
		             (first-goal-vars (extract-variables first-goal))
					 (t (set-intersection first-goal-vars temp))
					 (first-goal-code (goal-to-wam-x first-goal perm t))
					 (rest-code (goals-to-wam-x (cdr goals) perm temp)))
				(append first-goal-code rest-code)))))
					 
		 
(define (rule-handle-variable-occurances code occured)
  (cond ((null? code) '())
        (else (let* ((first-line (car code))
		             (first-label (car first-line))
					 (first-instr (cdr first-line))
					 (first-instr-type (first first-instr)))
				(cond ((equal? first-instr-type 'get_var) (let* ((variable (second first-instr))
				                                                  (rest (cdr (cdr first-instr)))
				                                                  (this-occured (set-contains? occured variable)))
															 (cond (this-occured (cons (cons first-label (append (list 'get_value variable) rest)) (rule-handle-variable-occurances (cdr code) occured)))
															       (else (cons (cons first-label (append (list 'get_variable variable) rest)) (rule-handle-variable-occurances (cdr code) (cons variable occured)))))))
					  ((equal? first-instr-type 'unify_var) (let* ((variable (second first-instr))
				                                                  (rest (cdr (cdr first-instr)))
				                                                  (this-occured (set-contains? occured variable)))
															 (cond (this-occured (cons (cons first-label (append (list 'unify_value variable) rest)) (rule-handle-variable-occurances (cdr code) occured)))
															       (else (cons (cons first-label (append (list 'unify-variable variable) rest)) (rule-handle-variable-occurances (cdr code) (cons variable occured)))))))
					  ((equal? first-instr-type 'put_var) (let* ((variable (second first-instr))
				                                                  (rest (cdr (cdr first-instr)))
				                                                  (this-occured (set-contains? occured variable)))
															 (cond (this-occured (cons (cons first-label (append (list 'put_value variable) rest)) (rule-handle-variable-occurances (cdr code) occured)))
															       (else (cons (cons first-label (append (list 'put_variable variable) rest)) (rule-handle-variable-occurances (cdr code) (cons variable occured)))))))
					  ((equal? first-instr-type 'set_var) (let* ((variable (second first-instr))
				                                                  (rest (cdr (cdr first-instr)))
				                                                  (this-occured (set-contains? occured variable)))
															 (cond (this-occured (cons (cons first-label (append (list 'set_value variable) rest)) (rule-handle-variable-occurances (cdr code) occured)))
															       (else (cons (cons first-label (append (list 'set_variable variable) rest)) (rule-handle-variable-occurances (cdr code) (cons variable occured)))))))
					  ((equal? first-instr-type 'call) (cons first-line (rule-handle-variable-occurances (cdr code) (only-perm occured))))
					  (else (cons first-line (rule-handle-variable-occurances (cdr code) occured))))))))
		 
(define (only-perm l)
  (cond ((null? l) '())
        ((equal? (car (car l)) 'Y) (cons (car l) (only-perm (cdr l))))
		(else (only-perm (cdr l)))))

(define (rule-to-terms rule)
  (cons (second rule) (split-body (third rule))))
(define (split-body body)
  (cond ((conjunction? body) (append (split-body (second body)) (split-body (third body))))
        (else (list body))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Uebersetzung einer Query (?- ...) in WAM-Code (sehr aehnlich zur Uebersetzung einer Regel, aber: Alle Variablen werden als permanent angesehen, da sie zur spaeteren Ausgabe benoetigt werden koennten. allocate wird durch allocate_q ersetzt, was das erste Environment setzen wird. deallocate wird durch deallocate_q ersetzt, welches spaeter die Ausgabe der Variablenbelegungen und das vom Nutzerverhalten abhaengige optionale Backtracking uebernimmt. deallocate_q bekommt die Liste der permanenten Variablen als Argument (wird benoetigt, um die externen Namen in interne Variblen zu uebersetzen).)
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (query-to-wam query)
  (let* ((perm (extract-variables query))
		 (temp '())
		 (terms-x (query-to-terms query))
		 (cut-expanded (handle-deepcuts terms-x (length perm)))
		 (terms (car cut-expanded))
		 (cut-precode (cdr cut-expanded))
		 (allocation-lines (cons (cons 0 (list 'allocate_q (+ (length cut-precode) (length perm)))) cut-precode))
		 (goals-wam-x (goals-to-wam-x terms perm temp))
		 (deallocation-lines (list (cons 0 (list 'deallocate_q perm))))
	     (wam-x (append allocation-lines goals-wam-x deallocation-lines)))
	(rule-handle-variable-occurances wam-x '())))
	
(define (query-to-terms query)
  (split-body (second query)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Extraktion von permanenten und temporaeren Variablen
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (perm-and-temp-variables clause); Gibt Paar aus (Liste mit permanenten Variablen) und (Liste mit temporaeren Variablen) der Klausel zurueck.
  (cond ((fact? clause) (cons '() (extract-variables clause)))
        ((question? clause) (perm-and-temp-variables-q clause))
		((rule? clause) (perm-and-temp-variables-r clause))
		(else (display "Prolog-Compile-Error: Keine Klausel!"))))

(define (perm-and-temp-variables-q question)
  (let ((body (second question)))
    (cond ((conjunction? body) (perm-and-temp-variables-rek '() (extract-variables (second body)) (third body)))
	      (else (cons '() (extract-variables body))))))

(define (perm-and-temp-variables-r rule)
  (let ((body (third rule)))
    (cond ((conjunction? body) (perm-and-temp-variables-rek '() (set-pair-union (extract-variables (second rule)) (extract-variables (second body))) (third body)))
	      (else (cons '() (extract-variables rule))))))

(define (perm-and-temp-variables-rek perm temp rest)
  (cond ((not (conjunction? rest)) (merge-perm-temp perm temp '() (extract-variables rest)))
        (else (let* ((n-perm-and-temp (perm-and-temp-variables-rek perm temp (second rest)))
		             (n-perm (car n-perm-and-temp))
					 (n-temp (cdr n-perm-and-temp)))
				(perm-and-temp-variables-rek n-perm n-temp (third rest))))))
  
(define (extract-variables term)
  (cond ((and (var? term) (not (equal? (cdr term) "_"))) (list term))
        ((complex-term? term) (set-union (map extract-variables (cdr term))))
		(else '())))
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Kompletter Compiler und Linker
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (compile-database code)
  (let* ((parsed-code (parse-prolog code))
		 (split-code (split-by-functor parsed-code))
		 (wam (sorted-clauses-to-wam split-code)))
	wam))
	
(define (compile-query code)
  (let* ((parsed-code (parse-prolog code))
		 (wam (query-to-wam (first parsed-code))))
	wam))