;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Datenstrukturen fuer den WAM-Interpreter
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Ein einfaches Register, das einen Wert enthalten kann.
(define (make-register)
  (let ((contents '*unassigned*))
    (define (dispatch message)
	  (cond ((eq? message 'get) contents)
	        ((eq? message 'set) (lambda (value)
			                      (set! contents value)))
			(else (display "Unbekannte Anfrage an Register"))))
	dispatch))
(define (get-contents register) (register 'get))
(define (set-contents! register value) ((register 'set) value))
(define (increment register) (set-contents! register (add1 (get-contents register))))

; Ein Keller.
(define (make-pdl)
  (let ((s '()))
    (define (push x) (set! s (cons x s)))
	(define (pop)
	  (cond ((null? s) #f)
	        (else (let ((top (car s)))
			        (set! s (cdr s))
					top))))
	(define (empty?) (null? s))
	(define (initialize) (set! s '()) #t)
	(define (dispatch message)
	  (cond ((eq? message 'push) push)
	        ((eq? message 'pop) (pop))
			((eq? message 'empty?) (empty?))
			((eq? message 'initialize) (initialize))
			(else (display "Unbekannte Anfrage an PDL"))))
	dispatch))
(define (pop stack) (stack 'pop))
(define (push stack value) ((stack 'push) value))
(define (empty? stack) (stack 'empty?))

; Ein Array, welches fuer den Anwender unendlich gross wirkt. 
(define (make-infinite-array)
  (let ((arr (make-vector 2 '*unassigned*))
        (size 2))
    (define (set i x)
	  (cond ((< i size) (vector-set! arr i x))
	        (else (set! arr (vector-append arr (make-vector size '*unassigned*))) (set! size (* 2 size)) (set i x))))
	(define (get i)
	  (cond ((< i size) (vector-ref arr i))
	        (else (set! arr (vector-append arr (make-vector size '*unassigned*))) (set! size (* 2 size)) (get i))))
	(define (dispatch message)
	  (cond ((eq? message 'get) get)
	        ((eq? message 'set) set)
			(else (display "Unbekannte Anfrage an Unendliches Array"))))
	dispatch))
(define (set-element! i-array i x) ((i-array 'set) i x))
(define (get-element i-array i) ((i-array 'get) i))
	
			
			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Interpreter fuer gelinkten WAM-Code
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (wam-interpreter code startline)
  ; Erstelle Register, die PDL und die Speicherbereiche.
  (let ((P (make-register))
		(CP (make-register))
		(S (make-register))
		(H (make-register))
		(HB (make-register))
		(B0 (make-register))
		(B (make-register))
		(E (make-register))
		(TR (make-register))
		(NUM-OF-ARGS (make-register))
		(FAIL (make-register))
		(MODE (make-register))
		(PDL (make-pdl))
		(HEAP (make-infinite-array))
		(ARGUMENT-REGISTERS (make-infinite-array))
		(STACK (make-infinite-array))
		(TRAIL (make-infinite-array)))
	
	; Operationen, mit denen der Speicher einfach addressiert werden kann (egal, wo die addr liegt).
	(define (store-get addr)
	  (case (first addr)
	    ((A X) (get-element ARGUMENT-REGISTERS (cdr addr)))
		((Y) (get-element STACK (+ (get-contents E) 2 (cdr addr))))
		((heap) (get-element HEAP (cdr addr)))))
	(define (store-set addr x)
	  (case (first addr)
	    ((A X) (set-element! ARGUMENT-REGISTERS (cdr addr) x))
		((Y) (set-element! STACK (+ (get-contents E) 2 (cdr addr)) x))
		((heap) (set-element! HEAP (cdr addr) x))))
	
	; Argument-Register in Choice-Point-Frame kopieren und anders rum
	(define (to-cpf newB n)
	  (to-cpf-rek 1 newB n))
	(define (to-cpf-rek i newB n)
	  (cond ((<= i n) (set-element! STACK (+ newB i) (store-get (cons 'A i)))
											   (to-cpf-rek (add1 i) newB n))
			(else #t)))
	(define (from-cpf B n)
	  (from-cpf-rek 1 B n))
	(define (from-cpf-rek i B n)
	  (cond ((<= i n) (store-set (cons 'A i) (get-element STACK (+ B i)))
											   (from-cpf-rek (add1 i) B n))
			(else #t)))
			
	; Lege ungebundene REF-Zellen auf den Heap.
	(define (heap-push-n n)
	  (heap-push-n-rek (get-contents H) (+ (get-contents H) n)))
	(define (heap-push-n-rek i e)
	  (cond ((< i e) (set-element! HEAP i (cons 'REF (cons 'heap i)))
	                 (heap-push-n-rek (add1 i) e))
	        (else #f)))
			
	; Fuehrt die While-Schleife von tidy_trail aus
	(define (tidy_from i)
	  (cond ((< i (get-contents TR)) (cond ((< (cdr (get-element TRAIL i)) (get-contents HB)) (tidy_from (add1 i)))
										   (else (set-element! TRAIL i (get-element TRAIL (subtract1 (get-contents TR)))) (tidy_from i))))))
										   
	; For-Schleife von unify
	(define (push-pdl v1 v2 i n)
	  (cond ((< i n) (push PDL (cons 'heap (+ (cdr v1) i))) (push PDL (cons 'heap (+ (cdr v2) i))) (push-pdl v1 v2 (add1 i) n))
	        ((= i n) (push PDL (cons 'heap (+ (cdr v1) i))) (push PDL (cons 'heap (+ (cdr v2) i))))))
	
	; Die While-Schleife von unify.
	(define (unify-loop)
	  (cond ((not (or (empty? PDL) (get-contents FAIL))) 
	    (let ((d1 (I-deref (pop PDL))) (d2 (I-deref (pop PDL))))
		  (cond ((not (equal? d1 d2))
			(let* ((s1 (store-get d1)) (s2 (store-get d2)) (t1 (car s1)) (v1 (cdr s1)) (t2 (car s2)) (v2 (cdr s2)))
			  (cond ((eq? t1 'REF) (I-bind d1 d2))
			        (else (case t2
					        ((REF) (I-bind d1 d2))
							((STR) (cond ((not (eq? t1 'STR)) (set-contents! FAIL #t))
							             ((not (equal? (store-get v1) (store-get v2))) (set-contents! FAIL #t))
										 (else (let ((n (cdr (store-get v1)))) (push-pdl v1 v2 1 n))))))))))))
		(unify-loop))))
		
			
	; Gibt erfuellende Belegung aus.
	(define (print-results perm)
	  (cond ((null? perm) (display "yes") (newline) #f)
	        (else (print-results-rek perm 1) #t)))
	(define (print-results-rek perm i)
	  (cond ((null? perm) #t)
	        (else (display (cdr (car perm))) (display " = ") (print-term (cons 'Y i)) (newline)
			      (print-results-rek (cdr perm) (add1 i)))))
	(define (print-term cell)
	  (let ((derefed (I-deref cell)))
	    (case (car (store-get derefed))
	      ((REF) (display "INT_VAR") (display (cdr (cdr (store-get derefed)))))
		  ((STR) (let ((functor (cdr (car (store-get (cdr (store-get derefed))))))
					   (arity (cdr (store-get (cdr (store-get derefed))))))
					(display functor) (display "(")
					(print-args-rek (cdr (cdr (store-get derefed))) 1 arity)
					(display ")"))))))
	(define (print-args-rek func-pos next-arg-pos arity)
	  (cond ((= next-arg-pos arity) (print-term (cons 'heap (+ func-pos next-arg-pos))))
	        ((< next-arg-pos arity) (print-term (cons 'heap (+ func-pos next-arg-pos))) (display ", ") (print-args-rek func-pos (add1 next-arg-pos) arity))))
	  
	
	; Hilfsfunktionen
	(define (I-deref addr)
	  (let* ((t-v (store-get addr))
	        (t (car t-v))
			(v (cdr t-v)))
		(cond ((and (eq? t 'REF) (not (equal? v addr))) (I-deref v))
		      (else addr))))
			  
	(define (I-bind a1 a2)
	  (cond ((eq? (car (store-get a1)) 'REF) (store-set a1 (store-get a2)) (I-trail a1))
	        ((eq? (car (store-get a2)) 'REF) (store-set a2 (store-get a1)) (I-trail a2))
			(else (display "Prolog-Error: Bind ohne REF-Zelle"))))
	
	(define (I-backtrack)
	  (cond ((equal? (get-contents B) -1) (display "no") (newline) (set-contents! P #f))
			(else (set-contents! B0 (get-element STACK (+ (get-contents B) (get-element STACK (get-contents B)) 7)))
				  (set-contents! P (get-element STACK (+ (get-contents B) (get-element STACK (get-contents B)) 4))))))
	  
	(define (I-unify a1 a2)
	  (begin
	    (PDL 'initialize)
	    (push PDL a1) (push PDL a2)
		(set-contents! FAIL #f)
		(unify-loop)))
	  
	(define (I-unwind_trail i1 i2)
	  (cond ((< i1 i2) (store-set (get-element TRAIL i1) (cons 'REF (get-element TRAIL i1))) (I-unwind_trail (add1 i1) i2))))
	  
	(define (I-tidy_trail)
	  (cond ((equal? (get-contents B) -1) (set-contents! TR 0)); Wenn kein Choice-Point-Frame mehr vorhanden ist, sollte die trail leer sein.
			(else (let ((i (get-element STACK (+ (get-contents B) (get-element STACK (get-contents B)) 5))))
					(tidy_from i)))))
	  
	(define (I-trail a)
	  (cond ((not (eq? (car a) 'heap)) (display "Prolog-Error: REF-Zelle zeigt nicht auf Heap."))
	        ((< (cdr a) (get-contents HB)) (set-element! TRAIL (get-contents TR) a)
										   (increment TR))))
	
	; WAM Operationen
	(define (I-put_structure fn reg)
	  (begin (set-element! HEAP (get-contents H) (cons 'STR (cons 'heap (add1 (get-contents H)))))
	         (set-element! HEAP (add1 (get-contents H)) fn)
			 (store-set reg (get-element HEAP (get-contents H)))
			 (set-contents! H (+ (get-contents H) 2))
			 (increment P)))
			 
	(define (I-set_variable reg)
	  (begin (set-element! HEAP (get-contents H) (cons 'REF (cons 'heap (get-contents H))))
	         (store-set reg (get-element HEAP (get-contents H)))
			 (increment H)
			 (increment P)))
			 
	(define (I-set_value reg)
	  (begin (set-element! HEAP (get-contents H) (store-get reg))
			 (increment H)
			 (increment P)))
			 
	(define (I-get_structure fn reg)
	  (let ((addr (I-deref reg))) (set-contents! FAIL #f) (set-contents! S 1)
	    (case (car (store-get addr))
		  ((REF) (set-element! HEAP (get-contents H) (cons 'STR (cons 'heap (add1 (get-contents H)))))
		         (set-element! HEAP (add1 (get-contents H)) fn)
				 (I-bind addr (cons 'heap (get-contents H)))
				 (set-contents! H (+ (get-contents H) 2))
				 (set-contents! MODE 'WRITE))
		  ((STR) (cond ((equal? (store-get (cdr (store-get addr))) fn) (set-contents! S (add1 (cdr (cdr (store-get addr))))) 
		                                             (set-contents! MODE 'READ))
		               (else (set-contents! FAIL #t))))
		  (else (set-contents! FAIL #t)))
		(cond ((get-contents FAIL) (I-backtrack))
		      (else (increment P)))))
	
	(define (I-unify-variable reg)
	  (begin
	    (case (get-contents MODE)
	      ((READ) (store-set reg (get-element HEAP (get-contents S))))
		  ((WRITE) (set-element! HEAP (get-contents H) (cons 'REF (cons 'heap (get-contents H))))
		           (store-set reg (get-element HEAP (get-contents H)))
				   (increment H)))
	    (increment S)
		(increment P)))
		
	(define (I-unify_value reg)
	  (begin
	    (set-contents! FAIL #f)
	    (case (get-contents MODE)
		  ((READ) (I-unify reg (cons 'heap (get-contents S))))
		  ((WRITE) (set-element! HEAP (get-contents H) (store-get reg))
		           (increment H)))
		(increment S)
		(cond ((get-contents FAIL) (I-backtrack))
		      (else (increment P)))))
		
	(define (I-call num arity)
	  (cond (num (set-contents! CP (add1 (get-contents P)))
	             (set-contents! NUM-OF-ARGS arity)
				 (set-contents! B0 (get-contents B))
				 (set-contents! P num))
			(else (I-backtrack))))
			
	(define (I-proceed)
	  (set-contents! P (get-contents CP)))
	  
	(define (I-put_variable reg1 reg2)
	  (begin (set-element! HEAP (get-contents H) (cons 'REF (cons 'heap (get-contents H))))
		     (store-set reg1 (get-element HEAP (get-contents H)))
		     (store-set reg2 (get-element HEAP (get-contents H)))
			 (increment H)
			 (increment P)))
	
	(define (I-put_value reg1 reg2)
	  (begin
	    (store-set reg2 (store-get reg1))
		(increment P)))
		
	(define (I-get_variable reg1 reg2)
	  (begin
	    (store-set reg1 (store-get reg2))
		(increment P)))
		
	(define (I-get_value reg1 reg2)
	  (begin
	    (set-contents! FAIL #f)
		(I-unify reg1 reg2)
		(cond ((get-contents FAIL) (I-backtrack))
		      (else (increment P)))))
			  
	(define (I-allocate N)
	  (let ((newE (cond ((> (get-contents E) (get-contents B)) (+ (get-contents E) (get-element STACK (+ 2 (get-contents E))) 3))
	                    (else (+ (get-contents B) (get-element STACK (get-contents B)) 8)))))
		(set-element! STACK newE (get-contents E))
		(set-element! STACK (+ newE 1) (get-contents CP))
		(set-element! STACK (+ newE 2) N)
		(set-contents! E newE)
		(increment P)))
		
	(define (I-deallocate)
	  (begin
	    (set-contents! P (get-element STACK (+ (get-contents E) 1)))
		(set-contents! E (get-element STACK (get-contents E)))))
		
	(define (I-try_me_else L)
	  (let ((newB (cond ((> (get-contents E) (get-contents B)) (+ (get-contents E) (get-element STACK (+ 2 (get-contents E))) 3))
	                    (else (+ (get-contents B) (get-element STACK (get-contents B)) 8)))))
		(set-element! STACK newB (get-contents NUM-OF-ARGS))
		(to-cpf newB (get-contents NUM-OF-ARGS))
		(set-element! STACK (+ newB (get-contents NUM-OF-ARGS) 1) (get-contents E))
		(set-element! STACK (+ newB (get-contents NUM-OF-ARGS) 2) (get-contents CP))
		(set-element! STACK (+ newB (get-contents NUM-OF-ARGS) 3) (get-contents B))
		(set-element! STACK (+ newB (get-contents NUM-OF-ARGS) 4) L)
		(set-element! STACK (+ newB (get-contents NUM-OF-ARGS) 5) (get-contents TR))
		(set-element! STACK (+ newB (get-contents NUM-OF-ARGS) 6) (get-contents H))
		(set-element! STACK (+ newB (get-contents NUM-OF-ARGS) 7) (get-contents B0))
		(set-contents! B newB)
		(set-contents! HB (get-contents H))
		(increment P)))
		
	(define (I-retry_me_else L)
	  (let ((n (get-element STACK (get-contents B))))
	    (from-cpf (get-contents B) n)
		(set-contents! E (get-element STACK (+ (get-contents B) n 1)))
		(set-contents! CP (get-element STACK (+ (get-contents B) n 2)))
		(set-element! STACK (+ (get-contents B) n 4) L)
		(I-unwind_trail (get-element STACK (+ (get-contents B) n 5)) (get-contents TR))
		(set-contents! TR (get-element STACK (+ (get-contents B) n 5)))
		(set-contents! H (get-element STACK (+ (get-contents B) n 6)))
		(set-contents! HB (get-contents H))
		(increment P)))
		
	(define (I-trust_me)
	  (let ((n (get-element STACK (get-contents B))))
	    (from-cpf (get-contents B) n)
		(set-contents! E (get-element STACK (+ (get-contents B) n 1)))
		(set-contents! CP (get-element STACK (+ (get-contents B) n 2)))
		(I-unwind_trail (get-element STACK (+ (get-contents B) n 5)) (get-contents TR))
		(set-contents! TR (get-element STACK (+ (get-contents B) n 5)))
		(set-contents! H (get-element STACK (+ (get-contents B) n 6)))
		(set-contents! HB (get-element STACK (+ (get-contents B) n 6))) ; Andere Reihenfolge als im WAMBOOK, aber so macht es mehr Sinn.
		(set-contents! B (get-element STACK (+ (get-contents B) n 3)))
		(increment P)))
		
	(define (I-set_void n)
	  (begin
	    (heap-push-n n)
		(set-contents! H (+ (get-contents H) n))
		(increment P)))
		
	(define (I-unify_void n)
	  (begin 
	    (case (get-contents MODE)
		  ((READ) (set-contents! S (+ (get-contents S) n)))
		  ((WRITE) (heap-push-n n)
		           (set-contents! H (+ (get-contents H) n))))
		(increment P)))
		
	(define (I-neck_cut)
	  (cond ((> (get-contents B) (get-contents B0)) (set-contents! B (get-contents B0))
													(I-tidy_trail)
													(increment P))
			(else (increment P))))
			
	(define (I-get_level reg)
	  (begin
	    (store-set reg (get-contents B0))
		(increment P)))
		
	(define (I-cut reg)
	  (cond ((> (get-contents B) (store-get reg)) (set-contents! B (store-get reg))
												  (I-tidy_trail)
												  (increment P))
			(else (increment P))))
			
	(define (I-allocate_q N)
	  (let ((newE 0))
		(set-element! STACK newE '*bottom*)
		(set-element! STACK (+ newE 1) (get-contents CP))
		(set-element! STACK (+ newE 2) N)
		(set-contents! E newE)
		(increment P)))
		
	(define (I-deallocate_q perm)
	  (begin
	    (cond ((print-results perm) (cond ((expect-enter-or-semicolon) (I-backtrack)); Fuehrt dazu, dass nach einer weiteren Loesung gesucht wird.
										(else (set-contents! P #f)))); Fuehrt dazu, dass das Programm beendet wird.
			  (else (set-contents! P #f)))))
	
	; Eine Zeile WAM-Code ausfuehren.
	(define (execute-line line)
	  (case (first line)
	    ((put_structure) (I-put_structure (second line) (third line)))
		((set_variable) (I-set_variable (second line)))
		((set_value) (I-set_value (second line)))
		((get_structure) (I-get_structure (second line) (third line)))
		((unify-variable) (I-unify-variable (second line)))
		((unify_value) (I-unify_value (second line)))
		((call) (I-call (second line) (third line)))
		((proceed) (I-proceed))
		((put_variable) (I-put_variable (second line) (third line)))
		((put_value) (I-put_value (second line) (third line)))
		((get_variable) (I-get_variable (second line) (third line)))
		((get_value) (I-get_value (second line) (third line)))
		((allocate) (I-allocate (second line)))
		((deallocate) (I-deallocate))
		((try_me_else) (I-try_me_else (second line)))
		((retry_me_else) (I-retry_me_else (second line)))
		((trust_me) (I-trust_me))
		((set_void) (I-set_void (second line)))
		((unify_void) (I-unify_void (second line)))
		((neck_cut) (I-neck_cut))
		((get_level) (I-get_level (second line)))
		((cut) (I-cut (second line)))
		((allocate_q) (I-allocate_q (second line)))
		((deallocate_q) (I-deallocate_q (second line)))))
		
	; Ausfuehren des WAM-Codes
	(define (run)
	  (cond ((get-contents P) (execute-line (vector-ref code (get-contents P))) (run))))
	; Initialisieren der Register, die initialisiert werden muessen.
	(set-contents! P startline)
	(set-contents! H 0)
	(set-contents! HB -1)
	(set-contents! B -1)
	(set-contents! TR 0)
	(set-contents! B0 -1)
	(set-contents! FAIL #f)
	run))