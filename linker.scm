;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Linken des WAM-Codes: Die Label werden entfernt und Referenzen werden durch die entsprechenden Zeilennummern (oder #f falls nicht vorhanden) ersetzt. (Bei call wird auch die Stelligkeit gespeichert.) Gelinkter WAM-Code kann von unserem Interpreter ausgefuehrt werden.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (pre-link wam-program)
  (let* ((split-wam (split-list-of-pairs wam-program))
		 (translation-table (extract-translation-table (car split-wam))))
	(cons (list->vector (replace-references (cdr split-wam) translation-table)) translation-table)))
	
(define (extract-translation-table label-list)
  (extract-translation-table-rek label-list 0))

(define (extract-translation-table-rek label-list pos)
  (cond ((null? label-list) '())
        (else (let* ((first-label (car label-list))
		             (rest (cdr label-list))
					 (relevant (not (equal? first-label 0)))
					 (rest-extracted (extract-translation-table-rek rest (add1 pos))))
				(cond (relevant (cons (cons first-label pos) rest-extracted))
				      (else rest-extracted))))))

(define (replace-references code-list translation-table)
  (cond ((null? code-list) '())
        (else (let* ((first-line (car code-list))
		             (rest-lines (cdr code-list))
					 (rest-replaced (replace-references rest-lines translation-table))
					 (first-instr (car first-line)))
				(case first-instr
				  ((try_me_else retry_me_else) (cons (list first-instr (label-lookup (second first-line) translation-table)) rest-replaced))
				  ((call) (cons (list first-instr (label-lookup (second first-line) translation-table) (cdr (second first-line))) rest-replaced))
				  (else (cons first-line rest-replaced)))))))

(define (label-lookup ref translation-table)
  (cond ((null? translation-table) #f)
        ((equal? (car (car translation-table)) ref) (cdr (car translation-table)))
		(else (label-lookup ref (cdr translation-table)))))
		
(define (link-query wam-query translation-table)
  (list->vector (replace-references (cdr (split-list-of-pairs wam-query)) translation-table)))

; Verbindet ein vorgelinktes Programm und eine Query (noch als WAM-Code). Gibt ein Paar aus (gelinktem WAM-Code fuer Programm und Query) und (Startzeile) aus.
(define (link-query-to-program pre-linked-program translation-table query)
  (let* ((linked-query (link-query query translation-table))
         (complete-code (vector-append pre-linked-program linked-query))
		 (startline (vector-length pre-linked-program)))
	(cons complete-code startline)))
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Kompletter Linker
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
(define (link-wam database query)
  (let* ((pre-linked-db (pre-link database))
		 (linked-wam (link-query-to-program (car pre-linked-db) (cdr pre-linked-db) query)))
	linked-wam))